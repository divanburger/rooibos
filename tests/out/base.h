/**
 * unit base
 */

#ifndef base_h
#define base_h

#include <string>
#include <rooibos/lang.h>

using std::string;

/**
 * function sum(a : int, b : int) : int [extends Anything]
 **/
int32_t sum(int32_t a, int32_t b);

/**
 * function main() : Anything
 **/
void main();

/**
 * function bob() : Anything
 **/
void bob();

#endif /* base_h */
