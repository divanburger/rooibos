/**
 * unit good
 */

#include "good.h"

/**
 * class Animal extends Anything
 **/
Animal::Animal(string name) {
	this->name = name;
}

void Animal::makeSound() {
	println("<Animal sound>");
	println(name);
}

void Animal::random() {
	println("!random!");
}

/**
 * class Dog extends Animal
 **/
void Dog::makeSound() {
	println("Woof!");
	println(name);
}

/**
 * class Cat extends Animal
 **/
void Cat::makeSound() {
	println("Meow!");
	println(name);
}

/**
 * class Lion extends Animal
 **/
void Lion::makeSound() {
	println("Roar!");
	println(name);
}

void adoptPet(Animal pet) {
	println("Adopted");
	pet.makeSound();
}

int32_t fibonacci(int32_t a, int32_t b, int32_t n) {
	if ((n == 0)) {
		return a;
	}
	return fibonacci(b, (a + b), (n - 1));
}

int32_t main() {
	int32_t a = 5;
	int32_t b = 10;
	int32_t c = (a + b);
	println("Hello, World!");
	printNumber(71);
	printNumber(6.0);
	c = ((c + a) + b);
	println(c);
	c = fibonacci(0, 1, 7);
	println(c);
	Animal animal = Animal("Miko");
	animal.makeSound();
	Animal::random();
	Dog dog = Dog("Fetch");
	adoptPet(dog);
	Cat cat = Cat("Sharkie");
	adoptPet(cat);
	return 0;
}
