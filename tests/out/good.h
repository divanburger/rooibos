/**
 * unit good
 */

#ifndef good_h
#define good_h

#include <string>
#include <rooibos/lang.h>

using std::string;

/**
 * class Animal extends Anything
 **/
class Animal : public Anything {
	string name;

public:
	Animal(string name);

	void makeSound();

	static void random();
};

/**
 * class Dog extends Animal
 **/
class Dog : public Animal {

public:
	void makeSound();
};

/**
 * class Cat extends Animal
 **/
class Cat : public Animal {

public:
	void makeSound();
};

/**
 * class Lion extends Animal
 **/
class Lion : public Animal {

public:
	void makeSound();
};

/**
 * function adoptPet(pet : Cat|Dog) : Anything
 **/
void adoptPet(Animal pet);

/**
 * function fibonacci(a : int, b : int, n : int) : int [extends Anything]
 **/
int32_t fibonacci(int32_t a, int32_t b, int32_t n);

/**
 * function printNumber(number : float|int) : Anything
 **/
template <typename ParmType_number>
void printNumber(void number) {
	println("Number: ");
	println(number);
}

/**
 * function main() : int [extends Anything]
 **/
int32_t main();

#endif /* good_h */
