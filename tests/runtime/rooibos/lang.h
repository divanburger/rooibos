#include <iostream>
#include <string>

void println(std::string s) {
	std::cout << s << std::endl;
}

void println(double v) {
	std::cout << v << std::endl;
}

void println(int32_t v) {
	std::cout << v << std::endl;
}
