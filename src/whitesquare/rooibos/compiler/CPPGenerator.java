package whitesquare.rooibos.compiler;

import java.util.ArrayList;
import java.util.LinkedList;

import whitesquare.rooibos.compiler.nodes.Assignment;
import whitesquare.rooibos.compiler.nodes.BinaryOp;
import whitesquare.rooibos.compiler.nodes.Class;
import whitesquare.rooibos.compiler.nodes.Constructor;
import whitesquare.rooibos.compiler.nodes.ConstructorCall;
import whitesquare.rooibos.compiler.nodes.Field;
import whitesquare.rooibos.compiler.nodes.Function;
import whitesquare.rooibos.compiler.nodes.FunctionCall;
import whitesquare.rooibos.compiler.nodes.IfStatement;
import whitesquare.rooibos.compiler.nodes.Literal;
import whitesquare.rooibos.compiler.nodes.Method;
import whitesquare.rooibos.compiler.nodes.MethodCall;
import whitesquare.rooibos.compiler.nodes.Parameter;
import whitesquare.rooibos.compiler.nodes.Return;
import whitesquare.rooibos.compiler.nodes.ScopeAccess;
import whitesquare.rooibos.compiler.nodes.Statement;
import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;
import whitesquare.rooibos.compiler.nodes.Unit;
import whitesquare.rooibos.compiler.nodes.Variable;
import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
import whitesquare.rooibos.compiler.nodes.VariableValue;
import whitesquare.rooibos.compiler.types.Modifier;
import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.types.TypeAlias;
import whitesquare.rooibos.compiler.types.UnionType;
import whitesquare.rooibos.compiler.visitors.Visitor;

class CPPGenerator extends Visitor {
	private String	baseDirectory;

	private CodeWriter	header = null;
	private CodeWriter	impl = null;
	
	private CodeWriter  curImpl = null;
	
	private enum CPPAccess {PRIVATE, PROTECTED, PUBLIC};
	
	private LinkedList<String> scopes = new LinkedList<String>();
	private LinkedList<CPPAccess> accessStack = new LinkedList<CPPAccess>();

	CPPGenerator(ScopeManager scope, ErrorReporter reporter, String base) {
		super(scope, reporter);
		baseDirectory = base;
	}

	public void generate(Unit unit) {
		header = new CodeWriter(baseDirectory + "/" + unit.name + ".h");
		impl = new CodeWriter(baseDirectory + "/" + unit.name + ".cpp");
		curImpl = impl;

		unit.visit(this);

		header.close();
		impl.close();
	}
	
	private boolean isTypeResolvable(Type type) {
		if (!(type instanceof UnionType)) return true;
		
		Type common = ((UnionType)type).getCommonType();
		if (common == Type.TOP) return false;
		return true;
	}
	
	private String getTypeString(Type type) {
		if (type instanceof UnionType) {
			return getTypeString(((UnionType)type).getCommonType());
		}
		
		if (type.name.equals("int"))
			return "int32_t";
		if (type == Type.TOP)
			return "void";
		return type.getRealName();
	}
	
	private void printAccess(CodeWriter writer, CPPAccess access) {
		CPPAccess cur = accessStack.pop();
		if (cur != access) {
			writer.unindent();
			writer.skipLine();
			writer.writeln(access.name().toLowerCase() + ":");
			writer.indent();
		}
		accessStack.push(access);
	}

	@Override
	public void visitUnitEnter(Unit unit) {
		header.writeln("/**");
		header.writeln(" * unit " + unit.name);
		header.writeln(" */");
		header.skipLine();
		header.writeln("#ifndef " + unit.name + "_h");
		header.writeln("#define " + unit.name + "_h");
		header.skipLine();
		header.writeln("#include <string>");
		header.writeln("#include <rooibos/lang.h>");
		header.skipLine();
		header.writeln("using std::string;");
		header.skipLine();
		
		curImpl.writeln("/**");
		curImpl.writeln(" * unit " + unit.name);
		curImpl.writeln(" */");
		curImpl.skipLine();
		curImpl.writeln("#include \"" + unit.name + ".h\"");
		curImpl.skipLine();
	}

	@Override
	public void visitUnitExit(Unit unit) {
		header.skipLine();
		header.writeln("#endif /* " + unit.name + "_h */");
	}

	@Override
	public void visitFunctionEnter(Function function) {
		header.skipLine();
		header.writeln("/**");
		String description = "";
		boolean first = true;
		for (Variable parm : function.parameters) {
			if (first)
				first = false;
			else
				description += ", ";
			description += parm.name + " : " + parm.type.getDisplayName();
		}
		header.writeln(" * function " + function.name + "(" + description + ") : " + function.returnType);
		header.writeln(" **/");
		
		ArrayList<String> templateParms = new ArrayList<String>();
		
		for (Parameter parm : function.parameters) {
			if (isTypeResolvable(parm.type)) continue;
			parm.type.realName = "ParmType_" + parm.name;
			templateParms.add(parm.type.realName);
		}
		
		if (!templateParms.isEmpty()) {
			curImpl = header;
			
			String templateHeader = "template <";
			first = true;
			for (String parm : templateParms) {
				if (first)
					first = false;
				else
					templateHeader += ", ";
				templateHeader += "typename " + parm;
			}
			curImpl.writeln(templateHeader + ">");
		}
		String head = getTypeString(function.returnType) + " " + function.name + "(";
		
		first = true;
		for (Variable parm : function.parameters) {
			if (first)
				first = false;
			else
				head += ", ";
			head += getTypeString(parm.type) + " " + parm.name;
		}
		head += ")";

		if (curImpl != header) header.writeln(head + ";");
		curImpl.writeln(head+ " {");
		curImpl.indent();
	}

	@Override
	public void visitFunctionExit(Function function) {
		curImpl.unindent();
		curImpl.writeln("}");
		curImpl.skipLine();
		
		curImpl = impl;
	}
	
	@Override
	public void visitMethodEnter(Method method) {
		printAccess(header, CPPAccess.PUBLIC);
		
		ArrayList<String> templateParms = new ArrayList<String>();
		
		for (Parameter parm : method.parameters) {
			if (isTypeResolvable(parm.type)) continue;
			parm.type.realName = "ParmType_" + parm.name;
			templateParms.add(parm.type.realName);
		}
		
		if (!templateParms.isEmpty()) {
			curImpl = header;
			
			String templateHeader = "template <";
			boolean first = true;
			for (String parm : templateParms) {
				if (first)
					first = false;
				else
					templateHeader += ", ";
				templateHeader += "typename " + parm;
			}
			curImpl.writeln(templateHeader + ">");
		}
		
		String scopePrefix = "";
		for (String scope : scopes) {
			scopePrefix += scope + "::";
		}
		
		String modifiers = "";
		String returnType = getTypeString(method.returnType) + " ";
		String head = method.name + "(";
		
		if (method.modifiers.contains(Modifier.STATIC)) modifiers = "static ";
		
		boolean first = true;
		for (Variable parm : method.parameters) {
			if (first)
				first = false;
			else
				head += ", ";
			head += getTypeString(parm.type) + " " + parm.name;
		}
		head += ")";

		if (templateParms.isEmpty()) {
			header.writeln(modifiers + returnType + head + ";");
			header.skipLine();
			
			impl.writeln(returnType + scopePrefix + head + " {");
			impl.indent();
		}
		else {
			header.writeln(modifiers + returnType + head + " {");
			header.indent();
		}
	}

	@Override
	public void visitMethodExit(Method method) {
		curImpl.unindent();
		curImpl.writeln("}");
		curImpl.skipLine();
		
		curImpl = impl;
	}
	
	public void visitConstructorEnter(Constructor constructor) {
		printAccess(header, CPPAccess.PUBLIC);
		
		ArrayList<String> templateParms = new ArrayList<String>();
		
		for (Parameter parm : constructor.parameters) {
			if (isTypeResolvable(parm.type)) continue;
			parm.type.realName = "ParmType_" + parm.name;
			templateParms.add(parm.type.realName);
		}
		
		if (!templateParms.isEmpty()) {
			curImpl = header;
			
			String templateHeader = "template <";
			boolean first = true;
			for (String parm : templateParms) {
				if (first)
					first = false;
				else
					templateHeader += ", ";
				templateHeader += "typename " + parm;
			}
			curImpl.writeln(templateHeader + ">");
		}
		
		String scopePrefix = "";
		for (String scope : scopes) {
			scopePrefix += scope + "::";
		}
		
		String head = constructor.type.name + "(";
		
		boolean first = true;
		for (Variable parm : constructor.parameters) {
			if (first)
				first = false;
			else
				head += ", ";
			head += getTypeString(parm.type) + " " + parm.name;
		}
		head += ")";

		if (templateParms.isEmpty()) {
			header.writeln(head + ";");
			header.skipLine();
			
			impl.writeln(scopePrefix + head + " {");
			impl.indent();
		}
		else {
			header.writeln(head + " {");
			header.indent();
		}	
	}
	
	public void visitConstructorExit(Constructor constructor) {
		curImpl.unindent();
		curImpl.writeln("}");
		curImpl.skipLine();
		
		curImpl = impl;
	}

	@Override
	public void visitFunctionCallEnter(FunctionCall call) {
		curImpl.writelnMid(call.function.name + "(");
	}

	@Override
	public void visitFunctionCallBetweenArg(FunctionCall call) {
		curImpl.writelnMid(", ");
	}

	@Override
	public void visitFunctionCallExit(FunctionCall call) {
		curImpl.writelnMid(")");
	}

	@Override
	public void visitConstructorCallEnter(ConstructorCall call) {
		curImpl.writelnMid(call.klass.name + "(");
	}

	@Override
	public void visitConstructorCallBetweenArg(ConstructorCall call) {
		curImpl.writelnMid(", ");
	}

	@Override
	public void visitConstructorCallExit(ConstructorCall call) {
		curImpl.writelnMid(")");
	}
	
	@Override
	public void visitMethodCallEnter(MethodCall call) {
		curImpl.writelnMid(call.function.name + "(");
	}

	@Override
	public void visitMethodCallBetweenArg(MethodCall call) {
		curImpl.writelnMid(", ");
	}

	@Override
	public void visitMethodCallExit(MethodCall call) {
		curImpl.writelnMid(")");
	}
	
	@Override
	public void visitFieldEnter(Field field) {
		printAccess(header, CPPAccess.PRIVATE);
		curImpl = header;
		
		visitVariableEnter(field);	
	}

	@Override
	public void visitFieldExit(Field field) {
		visitVariableExit(field);
		
		curImpl = impl;
	}	
	
	@Override
	public void visitVariableEnter(Variable variable) {
		String str = "";

		if (variable.isConst()) str += "const ";
		str += getTypeString(variable.type) + " " + variable.name;

		if (variable.initial != null) str += " = ";

		curImpl.writelnStart(str);
	}

	@Override
	public void visitVariableExit(Variable variable) {
		curImpl.writelnEnd(";");
	}

	@Override
	public void visitAssignmentEnter(Assignment assignment) {
		curImpl.writelnStart("");
	}

	@Override
	public void visitIfBeforeCond(IfStatement ifStat) {
		curImpl.writelnStart("if (");
	}

	@Override
	public void visitIfAfterCond(IfStatement ifStat) {
		curImpl.writelnEnd(") {");
		curImpl.indent();
	}

	@Override
	public void visitIfBeforeElse(IfStatement ifStat) {
		curImpl.unindent();
		curImpl.writeln("}");
		curImpl.writeln("else {");
		curImpl.indent();
	}

	@Override
	public void visitIfExit(IfStatement ifStat) {
		curImpl.unindent();
		curImpl.writeln("}");
	}
	
	@Override
	public void visitAssignmentMid(Assignment assignment) {
		curImpl.writelnMid(" = ");
	}

	@Override
	public void visitAssignmentExit(Assignment assignment) {
		curImpl.writelnEnd(";");
	}

	@Override
	public void visitStatementEnter(Statement stat) {
		curImpl.writelnStart("");
	}

	@Override
	public void visitStatementExit(Statement stat) {
		curImpl.writelnEnd(";");
	}

	@Override
	public void visitReturnEnter(Return ret) {
		curImpl.writelnStart("return ");
	}

	@Override
	public void visitReturnExit(Return ret) {
		curImpl.writelnEnd(";");
	}

	@Override
	public void visitVariableValue(VariableValue value) {
		curImpl.writelnMid(value.variable.name);
	}

	@Override
	public void visitBinaryOpEnter(BinaryOp binaryOp) {
		curImpl.writelnMid("(");
	}

	@Override
	public void visitBinaryOpMid(BinaryOp binaryOp) {
		curImpl.writelnMid(" " + binaryOp.op.symbol + " ");
	}

	@Override
	public void visitBinaryOpExit(BinaryOp binaryOp) {
		curImpl.writelnMid(")");
	}

	@Override
	public void visitLiteral(Literal literal) {
		switch (literal.kind) {
			case INTEGER: curImpl.writelnMid(literal.name); break;
			case FLOAT: curImpl.writelnMid(literal.name); break;
			case STRING: curImpl.writelnMid(literal.name); break;
		}
	}

	public void visitScopeAccessEnter(ScopeAccess scopeAccess) {
		if (scopeAccess.scope instanceof Type) {			
			if (scopeAccess.scope == scope.getEnclosingType()) {
				curImpl.writelnMid("this->");
				return;
			}
		}
		
		if (scopeAccess.scope instanceof Variable)
			curImpl.writelnMid(scopeAccess.scope.name + ".");
		else
			curImpl.writelnMid(scopeAccess.scope.name + "::");
	}
	
	public void visitScopeAccessExit(ScopeAccess scopeAccess) {
	}
	
	public void visitVariableScopeAccessEnter(VariableScopeAccess scopeAccess) {
		if (scopeAccess.scope instanceof Type) {			
			if (scopeAccess.scope == scope.getEnclosingType()) {
				curImpl.writelnMid("this->");
				return;
			}
		}
		
		if (scopeAccess.scope instanceof Variable)
			curImpl.writelnMid(scopeAccess.scope.name + ".");
		else
			curImpl.writelnMid(scopeAccess.scope.name + "::");
	}
	
	public void visitVariableScopeAccessExit(VariableScopeAccess scopeAccess) {
	}
	
	public void visitClassEnter(Class klass) {
		header.skipLine();
		header.writeln("/**");
		header.writelnStart(" * class " + klass.name);
		if (klass.superType != null) header.writelnMid(" extends " + klass.superType.name);
		header.writelnEnd("");
		header.writeln(" **/");
		header.writelnStart("class " + klass.name);
		if (klass.superType != null) header.writelnMid(" : public " + klass.superType.name);
		header.writelnEnd(" {");
		header.indent();
		
		curImpl.skipLine();
		curImpl.writeln("/**");
		curImpl.writelnStart(" * class " + klass.name);
		if (klass.superType != null) curImpl.writelnMid(" extends " + klass.superType.name);
		curImpl.writelnEnd("");
		curImpl.writeln(" **/");
		
		scope.enter(klass);
		scopes.push(klass.name);
		accessStack.push(CPPAccess.PRIVATE);
	}
	
	public void visitClassExit(Class klass) {
		header.unindent();
		header.writeln("};");
		header.skipLine();
		
		accessStack.pop();
		scopes.pop();
		scope.leave(klass);
	}
	
	public void visitTypeAliasStatement(TypeAliasStatement typeAliasStatement) {
		TypeAlias alias = typeAliasStatement.alias;
		
		header.writeln("type " + alias.name + " = " + getTypeString(alias.aliasOf));
	}
}