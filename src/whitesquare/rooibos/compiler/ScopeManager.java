package whitesquare.rooibos.compiler;

import whitesquare.rooibos.compiler.types.Type;

import whitesquare.rooibos.compiler.nodes.Source;
import whitesquare.rooibos.compiler.nodes.Node;
import whitesquare.rooibos.compiler.nodes.Scope;
import whitesquare.rooibos.compiler.nodes.Variable;
import whitesquare.rooibos.compiler.nodes.Function;

public class ScopeManager {
	public Scope root = new Scope(Source.INTERNAL, "<ROOT>");

	public Scope current;

	public ScopeManager() {
		current = root;
	}

	public void add(Node node) {
		current.add(node);
	}
	
	public void add(Type type) {
		current.add(type);
	}

	public void enter(Scope scope) {
		current.add(scope);
		current = scope;
	}

	public void leave(Scope scope) {
		if (current.parent == null) throw new Error("Trying to leave the root node");
		if (current != scope) throw new Error("Trying to leave the wrong scope");
		current = current.parent;
	}
	
	public void leave() {
		if (current.parent == null) throw new Error("Trying to leave the root node");
		current = current.parent;
	}	
	
	public Node find(String name) {
		Scope c = current;
		while (c != null) {
			Node t = c.get(name);
			if (t != null) return t;
			c = c.parent;
		}
		return null;
	}	

	public Scope findScope(String name) {
		Scope c = current;
		while (c != null) {
			Node t = c.get(name);
			if (t != null && t instanceof Scope) return (Scope)t;
			c = c.parent;
		}
		return null;
	}	
	
	public Variable findVariable(String name) {
		Scope c = current;
		while (c != null) {
			Node t = c.get(name);
			if (t != null && t instanceof Variable) return (Variable)t;
			c = c.parent;
		}
		return null;
	}

	public Function findFunction(String name) {
		Scope c = current;
		while (c != null) {
			Node t = c.get(name);
			if (t != null && t instanceof Function) return (Function)t;
			c = c.parent;
		}
		return null;
	}
	
	public Type getEnclosingType() {
		Scope c = current;
		while (c != null) {
			if (c instanceof Type) return (Type)c;
			c = c.parent;
		}
		return null;
	}

	public Type findType(String name) {
		Scope c = current;
		while (c != null) {
			Type t = c.getType(name);
			if (t != null) return t;
			c = c.parent;
		}
		return null;
	}
}