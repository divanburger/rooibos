package whitesquare.rooibos.compiler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import whitesquare.rooibos.compiler.nodes.Source;

public class ErrorReporter {
	private class Entry implements Comparable<Object> {
		Source source;
		String msg;

		Entry(Source source, String msg) {
			this.source = source;
			this.msg = msg;
		}

		public int compareTo(Object other) {
			if (other instanceof Entry) return this.source.compareTo(((Entry)other).source);
			return -1;
		}
	}

	private List<Entry> entries = new ArrayList<Entry>();
	private int warningCount = 0;
	private int errorCount = 0;

	public ErrorReporter() {

	}

	public void warning(Source source, String msg) {
		entries.add(new Entry(source, msg));
		warningCount++;
	}

	public void error(Source source, String msg) {
		entries.add(new Entry(source, msg));
		errorCount++;
	}

	public void print() {
		Collections.sort(entries);
		for (Entry entry : entries) {
			String header = entry.source.file + ": ln " + entry.source.line + 
				":" + entry.source.column + " > ";
			
			String[] parts = entry.msg.split("\n");
			
			System.out.println(header + parts[0]);
			
			String indent = "";
			for (int i = 0; i < header.length(); i++) indent += " ";
			for (int i = 1; i < parts.length; i++) System.out.println(indent + parts[i]);
		}
	}
	
	public int getWarningCount() {
		return warningCount;
	}	
	
	public int getErrorCount() {
		return errorCount;
	}
}