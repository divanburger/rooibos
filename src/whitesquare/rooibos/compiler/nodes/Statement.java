package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class Statement extends Node {
	public Source source;
	public Scope parent = null;
	public String name;
	public Value value;

	public Statement(Source source, String name, Value value) {
		super(source, name);
		this.value = value;
	}

	// Visiting
	@Override
	public void visit(Visitor visitor) {
		visitor.visitStatementEnter(this);
		value.visit(visitor);
		visitor.visitStatementExit(this);
	}
	
	@Override
	public void printTree(String indent) {
		value.printTree(indent);
	}
	
	@Override
	public String toString() {
		return value == null ? "!ERROR!" : value.toString();
	}
}