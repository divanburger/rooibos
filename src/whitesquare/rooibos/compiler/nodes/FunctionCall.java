package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class FunctionCall extends Call {
	public FunctionCall(Source source, Function function) {
		super(source, function);
	}

	public void visit(Visitor visitor) {
		visitor.visitFunctionCallEnter(this);
		boolean first = true;
		for (Value arg : arguments) {
			if (first)
				first = false;
			else
				visitor.visitFunctionCallBetweenArg(this);
			arg.visit(visitor);
		}
		visitor.visitFunctionCallExit(this);
	}
	
	@Override
	public String toString() {
		return function.toString();
	}
}