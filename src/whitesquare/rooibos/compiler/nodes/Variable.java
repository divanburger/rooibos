package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Variable extends Value {
	public Value initial = null;

	static public Variable INVALID = new Variable(Source.INTERNAL, Type.INVALID, "<unknown>");

	public Variable(Source source, Type type, String name) {
		super(source, type, name);
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitVariableEnter(this);
		if (initial != null) initial.visit(visitor);
		visitor.visitVariableExit(this);
	}

	@Override
	public void printTree(String indent) {
		String str = indent + "[var] " + toString();
		if (initial != null) str += " =";
		System.out.println(str);

		if (initial != null) {
			initial.printTree(indent + "\t");
		}
	}
}