package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class BinaryOp extends Value {
	public enum Operation {
		ADD("+"), SUB("-"), MUL("*"), DIV("/"), EQUAL("==");
		public String symbol;
		Operation(String sym) {symbol = sym;}
	};
	
	public Operation op;
	public Value left = null;
	public Value right = null;

	public BinaryOp(Source source, Operation op, Value left, Value right) {
		super(source, left.type, "binaryop-"+op.name()+"-"+genID());
		this.op = op;
		this.left = left;
		this.right = right;
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + toString());
		indent += "\t";
		if (left != null) left.printTree(indent); else System.out.println(indent + "INVALID");
		if (right != null) right.printTree(indent); else System.out.println(indent + "INVALID");
	}

	public void visit(Visitor visitor) {
		visitor.visitBinaryOpEnter(this);
		left.visit(visitor);
		visitor.visitBinaryOpMid(this);
		right.visit(visitor);
		visitor.visitBinaryOpExit(this);
	}

	@Override
	public String toString() {
		String str = op.name() + " : " + type;
		if (isConst()) str += " [const]";
		return str;
	}
}