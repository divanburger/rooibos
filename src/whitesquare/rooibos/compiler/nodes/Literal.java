package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

import whitesquare.rooibos.compiler.types.Type;

public class Literal extends Value {
	public enum Kind {INTEGER, FLOAT, STRING}

	public Kind kind;

	public Literal(Source source, Type type, String str) {
		super(source, type, str);
		kind = Kind.STRING;
	}

	public Literal(Source source, Type type, long number) {
		super(source, type, Long.toString(number));
		kind = Kind.INTEGER;
	}

	public Literal(Source source, Type type, double number) {
		super(source, type, Double.toString(number));
		kind = Kind.FLOAT;
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitLiteral(this);
	}
}