package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.types.TypeAlias;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class TypeAliasStatement extends Statement {
	public TypeAlias alias;
	
	public TypeAliasStatement(Source source, TypeAlias alias) {
		super(source, alias.name, new Value(source, Type.BOTTOM));
		this.alias = alias;
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitTypeAliasStatement(this);
	}
}
