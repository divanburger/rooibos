package whitesquare.rooibos.compiler.nodes;

import org.antlr.v4.runtime.Token;

import whitesquare.rooibos.compiler.visitors.Visitor;

public abstract class Node {
	public Source source;
	public Scope parent = null;
	public String name;

	public Node(Token token) {
		this.source = new Source(token);
		this.name = token.getText();
	}

	public Node(Token token, String name) {
		this.source = new Source(token);
		this.name = name;
	}

	public Node(Source source, String name) {
		this.source = source;
		this.name = name;
	}
	
	public boolean isEquivalent(Node other) {
		return this == other;
	}
	
	public String getDisplayName() {
		return name;
	}

	// Visiting
	public abstract void visit(Visitor visitor);

	// Printing for debug purposes
	public void printTree(String indent) {
		System.out.println(indent + toString());
	}

	public String toString() {
		return "[" + name + "]";
	}

	// Generation of an ID to make node names unique
	private static long _id;
	public static long genID() {
		return _id++;
	}
}