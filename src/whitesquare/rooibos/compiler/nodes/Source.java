package whitesquare.rooibos.compiler.nodes;

import org.antlr.v4.runtime.Token;

public class Source implements Comparable<Object> {
	public String file;
	public int line;
	public int column;

	static public Source UNKNOWN = new Source(-1, -1);
	static public Source INTERNAL = new Source("<internal>", -1, -1);
	static public Source GENERATED = new Source("<generated>", -1, -1);

	public Source(String filename) {
		this(filename, -1, -1);
	}

	public Source(int line, int column) {
		this("<unknown>", line, column);
	}

	public Source(String filename, int line, int column) {
		this.file = filename;
		this.line = line;
		this.column = column;
	}

	public Source(Token token) {
		this.file = token.getTokenSource().getSourceName();
		this.line = token.getLine();
		this.column = token.getCharPositionInLine();
	}

	public int compareTo(Object to) {
		if (to instanceof Source) {
			Source other = (Source)to;
			int cfile = this.file.compareTo(other.file);
			if (cfile != 0) return cfile;
			if (this.line < other.line) return -1;
			if (this.line > other.line) return 1;
			if (this.column < other.column) return -1;
			if (this.column > other.column) return 1;
			return 0;
		}
		return -1;
	}
}