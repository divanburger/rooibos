package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Parameter extends Variable {
	public Parameter(Source source, Type type, String name) {
		super(source, type, name);
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitParameter(this);
	}

	@Override
	public void printTree(String indent) {
		String str = indent + "[parameter] " + toString();
		if (initial != null) str += " =";
		System.out.println(str);

		if (initial != null) {
			initial.printTree(indent + "\t");
		}
	}
}
