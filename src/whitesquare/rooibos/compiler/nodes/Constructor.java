package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Constructor extends Function {
	public Type type;
	
	public Constructor(Source source, Type type) {
		super(source, "constructor");
		this.type = type;
		returnType = type;
	}

	public Constructor copyWithSignature(Type newType) {
		Constructor constructor = new Constructor(source, newType);
		for (Parameter parm : parameters) constructor.addParameter(parm);
		return constructor;
	}
	
	public Constructor copyWithSignature() {
		return copyWithSignature(type);
	}
		
	@Override
	public void visit(Visitor visitor) {
		visitor.visitConstructorEnter(this);
		for (Node node : children)
			node.visit(visitor);
		visitor.visitConstructorExit(this);
	}

	@Override
	public String toString() {
		String str = "[constructor] " + name + "(";
		boolean first = true;
		for (Variable parm : parameters) {
			if (first)
				first = false;
			else
				str += ", ";
			str += parm;
		}
		return str + ") : " + returnType;
	}
}
