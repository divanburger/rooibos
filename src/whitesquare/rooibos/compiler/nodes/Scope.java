package whitesquare.rooibos.compiler.nodes;

import java.util.ArrayList;

import org.antlr.v4.runtime.Token;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Scope extends Node {
	protected ArrayList<Node>	children = new ArrayList<Node>();
	protected ArrayList<Type>	types = new ArrayList<Type>();

	public Scope(Source source, String name) {
		super(source, name);
	}

	public Scope(Token token, String name) {
		super(token, name);
	}

	public void add(Node node) {
		children.add(node);
		node.parent = this;
	}

	public void add(Type type) {
		types.add(type);
	}

	public Type getType(String name) {
		for (Type type : types)
			if (type.name.equals(name))
				return type;
		return null;
	}

	public Node get(String name) {
		for (Node node : children)
			if (node.name.equals(name))
				return node;
		return null;	
	}
	
	public Node find(String name) {
		Scope c = this;
		while (c != null) {
			Node t = c.get(name);
			if (t != null) return t;
			c = c.parent;
		}
		return null;
	}
	
	public Scope findScope(String name) {
		Node n = find(name);
		if (n instanceof Scope) return (Scope)n;
		return null;
	}
	
	public Variable findVariable(String name) {
		Node n = find(name);
		if (n instanceof Variable) return (Variable)n;
		return null;
	}

	@Override
	public void visit(Visitor visitor) {
		for (Node node : children)
			node.visit(visitor);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + toString());

		for (Node node : children) {
			node.printTree(indent + "\t");
		}
	}
}