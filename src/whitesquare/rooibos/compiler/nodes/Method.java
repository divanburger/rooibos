package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class Method extends Function {
	public Class klass;
	
	public Method(Source source, String name, Class klass) {
		super(source, name);
		this.klass = klass;
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitMethodEnter(this);
		for (Node node : children)
			node.visit(visitor);
		visitor.visitMethodExit(this);
	}

	@Override
	public String toString() {
		String str = "[method] " + name + "(";
		for (Variable parm : parameters)
			str += parm + ", ";
		return str + ") : " + returnType;
	}
}
