package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class Unit extends Scope {
	public Unit(String filename, String name) {
		super(new Source(filename), name);
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitUnitEnter(this);
		for (Node node : children)
			node.visit(visitor);
		visitor.visitUnitExit(this);
	}
}