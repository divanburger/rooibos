package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class IfStatement extends Scope {
	public Value condition;
	public Scope ifPart;
	
	public IfStatement(Source source) {
		super(source, "if-"+genID());
	}
	
	public void setCondition(Value condition) {
		this.condition = condition;
	}
		
	public void setIfPart(Scope ifPart) {
		this.ifPart = ifPart;
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitIfBeforeCond(this);
		condition.visit(visitor);
		visitor.visitIfAfterCond(this);
		ifPart.visit(visitor);
		visitor.visitIfExit(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + toString());
		condition.printTree(indent + "\t");

		for (Node node : children) {
			node.printTree(indent + "\t");
		}
	}
}
