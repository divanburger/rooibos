package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class ConstructorCall extends Call {
	public Class klass;
	
	public ConstructorCall(Source source, Class klass) {
		super(source, klass.findConstructor());
		this.klass = klass;
	}
	
	public void visit(Visitor visitor) {
		visitor.visitConstructorCallEnter(this);
		boolean first = true;
		for (Value arg : arguments) {
			if (first)
				first = false;
			else
				visitor.visitConstructorCallBetweenArg(this);
			arg.visit(visitor);
		}
		visitor.visitConstructorCallExit(this);
	}
	
	@Override
	public String toString() {
		return klass.name;
	}
}
