package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class Return extends Node {
	public Value value = null;

	public Return(Source source) {
		super(source, "return-"+genID());
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitReturnEnter(this);
		if (value != null) value.visit(visitor);
		visitor.visitReturnExit(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + "return");
		indent += "\t";
		if (value != null) value.printTree(indent); else System.out.println(indent + "INVALID");
	}
}