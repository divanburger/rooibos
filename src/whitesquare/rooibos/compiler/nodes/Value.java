package whitesquare.rooibos.compiler.nodes;

import java.util.Set;
import java.util.TreeSet;

import whitesquare.rooibos.compiler.types.Modifier;
import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Value extends Node {	
	public Type type = null;

	protected Set<Modifier> modifiers = new TreeSet<Modifier>();

	public Value(Source source, Type type) {
		this(source, type, "value-"+genID());
	}

	public Value(Source source, Type type, String name) {
		super(source, name);
		this.type = type;
	}

	public void addModifier(Modifier modifier) {
		modifiers.add(modifier);
	}

	public boolean hasModifier(Modifier modifier) {
		return modifiers.contains(modifier);
	}

	public void setConst() {
		addModifier(Modifier.CONST);
	}

	public boolean isConst() {
		return hasModifier(Modifier.CONST);
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitValue(this);
	}
	
	@Override
	public String toString() {
		String str = name + " : " + type;
		if (isConst()) str += " [const]";
		return str;
	}
}