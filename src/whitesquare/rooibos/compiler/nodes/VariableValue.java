package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Modifier;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class VariableValue extends Value {
	public Variable variable;
	
	public VariableValue(Source source, Variable variable) {
		super(source, variable.type, variable.name);
		this.variable = variable;
	}

	@Override
	public boolean hasModifier(Modifier modifier) {
		if (modifiers.contains(modifier))
			return true;

		return variable.hasModifier(modifier);
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitVariableValue(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + variable.toString());
	}
}