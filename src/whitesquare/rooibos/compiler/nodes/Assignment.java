package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class Assignment extends Node {
	public VariableValue left = null;
	public Value right = null;

	public Assignment(Source source) {
		super(source, "assign-"+genID());
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitAssignmentEnter(this);
		if (left != null) left.visit(visitor);
		visitor.visitAssignmentMid(this);
		if (right != null) right.visit(visitor);
		visitor.visitAssignmentExit(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + "assign");
		indent += "\t";
		if (left != null) left.printTree(indent); else System.out.println(indent + "INVALID");
		if (right != null) right.printTree(indent); else System.out.println(indent + "INVALID");
	}
}