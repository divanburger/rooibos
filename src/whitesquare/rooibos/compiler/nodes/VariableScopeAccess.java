package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class VariableScopeAccess extends VariableValue {
	public Node scope;
	public VariableValue variableValue;

	public VariableScopeAccess(Source source, Node scope, VariableValue variableValue) {
		super(source, variableValue.variable);
		this.scope = scope;
		this.variableValue = variableValue;
	}

	@Override
	public String toString() {
		return scope + " => " + variableValue.toString();
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitVariableScopeAccessEnter(this);
		variableValue.visit(visitor);
		visitor.visitVariableScopeAccessExit(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + "[access] " + scope);
		variableValue.printTree(indent + "\t");
	}
}
