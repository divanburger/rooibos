package whitesquare.rooibos.compiler.nodes;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import whitesquare.rooibos.compiler.types.Modifier;
import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Function extends Scope {
	public Type returnType = Type.TOP;
	public Set<Modifier> modifiers = new TreeSet<Modifier>();

	public ArrayList<Parameter> parameters = new ArrayList<Parameter>();

	public Function(Source source, String name) {
		super(source, name);
	}

	public void addParameter(Parameter parameter) {
		parameters.add(parameter);
		add(parameter);
	}
	
	public boolean isEquivalent(Node other) {
		if (!(other instanceof Function)) return false;
		Function otherFunc = (Function)other;
		if (returnType != otherFunc.returnType) return false;
		if (parameters.size() != otherFunc.parameters.size()) return false;
		for (int i = 0; i < parameters.size(); i++)
			if (!(parameters.get(i).type == otherFunc.parameters.get(i).type))
				return false;
		return true;
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitFunctionEnter(this);
		for (Node node : children)
			node.visit(visitor);
		visitor.visitFunctionExit(this);
	}

	@Override
	public String toString() {
		String str = name + "(";
		boolean first = true;
		for (Variable parm : parameters) {
			if (first)
				first = false;
			else
				str += ", ";
			str += parm.getDisplayName() + " : " + parm.type.getDisplayName();
		}
		return str + ") : " + returnType;
	}
}