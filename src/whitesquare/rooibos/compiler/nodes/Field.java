package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Field extends Variable {
	public Field(Source source, Type type, String name) {
		super(source, type, name);
	}

	@Override
	public void visit(Visitor visitor) {
		visitor.visitFieldEnter(this);
		if (initial != null) initial.visit(visitor);
		visitor.visitFieldExit(this);
	}

	@Override
	public void printTree(String indent) {
		String str = indent + "[field] " + toString();
		if (initial != null) str += " =";
		System.out.println(str);

		if (initial != null) {
			initial.printTree(indent + "\t");
		}
	}
}
