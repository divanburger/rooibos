package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.visitors.Visitor;

public class Class extends Type {
	public Class(Source source, String name) {
		super(source, name);
	}
		
	@Override
	public void visit(Visitor visitor) {
		visitor.visitClassEnter(this);
		for (Node node : children)
			node.visit(visitor);
		visitor.visitClassExit(this);
	}
}
