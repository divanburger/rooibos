package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class ScopeAccess extends Value {
	public Node scope;
	public Value value;
	
	public ScopeAccess(Source source, Node scope, Value value) {
		super(source, value.type);
		this.scope = scope;
		this.value = value;
	}

	@Override
	public String toString() {
		return scope + " => " + value.toString();
	}
	
	@Override
	public void visit(Visitor visitor) {
		visitor.visitScopeAccessEnter(this);
		value.visit(visitor);
		visitor.visitScopeAccessExit(this);
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + "[access] " + scope);
		value.printTree(indent + "\t");
	}
}
