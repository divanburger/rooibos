package whitesquare.rooibos.compiler.nodes;

import java.util.ArrayList;

import whitesquare.rooibos.compiler.types.Type;

public class Call extends Value {
	public Function function;
	public ArrayList<Value> arguments = new ArrayList<Value>();
	
	public Call(Source source, Function function) {
		super(source, function == null ? Type.INVALID : function.returnType);
		this.function = function;
	}

	@Override
	public void printTree(String indent) {
		System.out.println(indent + "[call] " + toString());
		indent += "\t";
		for (Value arg : arguments)
			arg.printTree(indent);
	}
}
