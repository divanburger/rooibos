package whitesquare.rooibos.compiler.nodes;

import whitesquare.rooibos.compiler.visitors.Visitor;

public class MethodCall extends Call {
	public MethodCall(Source source, Method method) {
		super(source, method);
	}

	public void visit(Visitor visitor) {
		visitor.visitMethodCallEnter(this);
		boolean first = true;
		for (Value arg : arguments) {
			if (first)
				first = false;
			else
				visitor.visitMethodCallBetweenArg(this);
			arg.visit(visitor);
		}
		visitor.visitMethodCallExit(this);
	}
	
	@Override
	public String toString() {
		return function.toString();
	}
}