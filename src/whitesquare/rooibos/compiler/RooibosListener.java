// Generated from grammar/Rooibos.g4 by ANTLR 4.0
package whitesquare.rooibos.compiler;

	import java.util.LinkedList;
	import java.util.Set;
	import java.util.TreeSet;

	import whitesquare.rooibos.compiler.nodes.Node;
	import whitesquare.rooibos.compiler.nodes.Scope;
	import whitesquare.rooibos.compiler.nodes.Constructor;
	import whitesquare.rooibos.compiler.nodes.Method;
	import whitesquare.rooibos.compiler.nodes.Function;
	import whitesquare.rooibos.compiler.nodes.Call;
	import whitesquare.rooibos.compiler.nodes.ConstructorCall;
	import whitesquare.rooibos.compiler.nodes.MethodCall;
	import whitesquare.rooibos.compiler.nodes.FunctionCall;
	import whitesquare.rooibos.compiler.nodes.Assignment;
	import whitesquare.rooibos.compiler.nodes.Statement;
	import whitesquare.rooibos.compiler.nodes.Return;
	import whitesquare.rooibos.compiler.nodes.Parameter;
	import whitesquare.rooibos.compiler.nodes.Field;
	import whitesquare.rooibos.compiler.nodes.ScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Variable;
	import whitesquare.rooibos.compiler.nodes.VariableValue;
	import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Value;
	import whitesquare.rooibos.compiler.nodes.Literal;
	import whitesquare.rooibos.compiler.nodes.BinaryOp;
	import whitesquare.rooibos.compiler.nodes.Source;
	import whitesquare.rooibos.compiler.nodes.IfStatement;
	import whitesquare.rooibos.compiler.nodes.Class;
	import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;

	import whitesquare.rooibos.compiler.types.Modifier;
	import whitesquare.rooibos.compiler.types.Type;
	import whitesquare.rooibos.compiler.types.TypeAlias;
	import whitesquare.rooibos.compiler.types.UnionType;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface RooibosListener extends ParseTreeListener {
	void enterVarPart(RooibosParser.VarPartContext ctx);
	void exitVarPart(RooibosParser.VarPartContext ctx);

	void enterExpression(RooibosParser.ExpressionContext ctx);
	void exitExpression(RooibosParser.ExpressionContext ctx);

	void enterCall(RooibosParser.CallContext ctx);
	void exitCall(RooibosParser.CallContext ctx);

	void enterAtom(RooibosParser.AtomContext ctx);
	void exitAtom(RooibosParser.AtomContext ctx);

	void enterCallStatement(RooibosParser.CallStatementContext ctx);
	void exitCallStatement(RooibosParser.CallStatementContext ctx);

	void enterVariableValue(RooibosParser.VariableValueContext ctx);
	void exitVariableValue(RooibosParser.VariableValueContext ctx);

	void enterReturnStatement(RooibosParser.ReturnStatementContext ctx);
	void exitReturnStatement(RooibosParser.ReturnStatementContext ctx);

	void enterBlock(RooibosParser.BlockContext ctx);
	void exitBlock(RooibosParser.BlockContext ctx);

	void enterType(RooibosParser.TypeContext ctx);
	void exitType(RooibosParser.TypeContext ctx);

	void enterTypeAtom(RooibosParser.TypeAtomContext ctx);
	void exitTypeAtom(RooibosParser.TypeAtomContext ctx);

	void enterTypeAlias(RooibosParser.TypeAliasContext ctx);
	void exitTypeAlias(RooibosParser.TypeAliasContext ctx);

	void enterParameter(RooibosParser.ParameterContext ctx);
	void exitParameter(RooibosParser.ParameterContext ctx);

	void enterFieldDeclaration(RooibosParser.FieldDeclarationContext ctx);
	void exitFieldDeclaration(RooibosParser.FieldDeclarationContext ctx);

	void enterStatements(RooibosParser.StatementsContext ctx);
	void exitStatements(RooibosParser.StatementsContext ctx);

	void enterEol(RooibosParser.EolContext ctx);
	void exitEol(RooibosParser.EolContext ctx);

	void enterFieldPart(RooibosParser.FieldPartContext ctx);
	void exitFieldPart(RooibosParser.FieldPartContext ctx);

	void enterModifiers(RooibosParser.ModifiersContext ctx);
	void exitModifiers(RooibosParser.ModifiersContext ctx);

	void enterMethodDeclaration(RooibosParser.MethodDeclarationContext ctx);
	void exitMethodDeclaration(RooibosParser.MethodDeclarationContext ctx);

	void enterVarDeclaration(RooibosParser.VarDeclarationContext ctx);
	void exitVarDeclaration(RooibosParser.VarDeclarationContext ctx);

	void enterArgument(RooibosParser.ArgumentContext ctx);
	void exitArgument(RooibosParser.ArgumentContext ctx);

	void enterScoping(RooibosParser.ScopingContext ctx);
	void exitScoping(RooibosParser.ScopingContext ctx);

	void enterConstructorDeclaration(RooibosParser.ConstructorDeclarationContext ctx);
	void exitConstructorDeclaration(RooibosParser.ConstructorDeclarationContext ctx);

	void enterClassStatement(RooibosParser.ClassStatementContext ctx);
	void exitClassStatement(RooibosParser.ClassStatementContext ctx);

	void enterCondition(RooibosParser.ConditionContext ctx);
	void exitCondition(RooibosParser.ConditionContext ctx);

	void enterClassDeclaration(RooibosParser.ClassDeclarationContext ctx);
	void exitClassDeclaration(RooibosParser.ClassDeclarationContext ctx);

	void enterIfStatement(RooibosParser.IfStatementContext ctx);
	void exitIfStatement(RooibosParser.IfStatementContext ctx);

	void enterStatement(RooibosParser.StatementContext ctx);
	void exitStatement(RooibosParser.StatementContext ctx);

	void enterModifier(RooibosParser.ModifierContext ctx);
	void exitModifier(RooibosParser.ModifierContext ctx);

	void enterAssignment(RooibosParser.AssignmentContext ctx);
	void exitAssignment(RooibosParser.AssignmentContext ctx);

	void enterTerm(RooibosParser.TermContext ctx);
	void exitTerm(RooibosParser.TermContext ctx);

	void enterFunctionDeclaration(RooibosParser.FunctionDeclarationContext ctx);
	void exitFunctionDeclaration(RooibosParser.FunctionDeclarationContext ctx);

	void enterVariable(RooibosParser.VariableContext ctx);
	void exitVariable(RooibosParser.VariableContext ctx);

	void enterRooibos(RooibosParser.RooibosContext ctx);
	void exitRooibos(RooibosParser.RooibosContext ctx);

	void enterPolynomial(RooibosParser.PolynomialContext ctx);
	void exitPolynomial(RooibosParser.PolynomialContext ctx);
}