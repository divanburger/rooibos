package whitesquare.rooibos.compiler;

import java.io.File;
import java.io.IOException;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;

import whitesquare.rooibos.compiler.nodes.Function;
import whitesquare.rooibos.compiler.nodes.Parameter;
import whitesquare.rooibos.compiler.nodes.Source;
import whitesquare.rooibos.compiler.nodes.Unit;
import whitesquare.rooibos.compiler.types.Type;
import whitesquare.rooibos.compiler.types.UnionType;
import whitesquare.rooibos.compiler.visitors.SemanticChecker;
import whitesquare.rooibos.compiler.visitors.TypeChecker;

public class Compiler {
	public Compiler(String filename) {
		System.out.println("Compiling: " + filename);

		// Extract name
		int startIndex = filename.lastIndexOf(File.separator);
		int extIndex = filename.lastIndexOf(".");
		String name = filename.substring(startIndex+1, extIndex);

		System.out.println("Name: " + name);

		ErrorReporter reporter = new ErrorReporter();

		Unit unit = new Unit(filename, name);
		ScopeManager scope = new ScopeManager();
		scope.enter(unit);

		Type intType = new Type("int");
		Type floatType = new Type("float");
		Type stringType = new Type("string");
		Type printableType = new UnionType(intType, floatType, stringType);

		scope.root.add(Type.TOP);
		scope.root.add(Type.BOTTOM);
		scope.root.add(Type.BOOLEAN);
		scope.root.add(intType);
		scope.root.add(floatType);
		scope.root.add(stringType);

		Function printFunc = new Function(Source.INTERNAL, "println");
		printFunc.addParameter(new Parameter(Source.INTERNAL, printableType, "value"));
		scope.root.add(printFunc);

		try {
			ANTLRFileStream stream = new ANTLRFileStream(filename);
			RooibosLexer lexer = new RooibosLexer(stream);
			RooibosParser parser = new RooibosParser(new CommonTokenStream(lexer));

			parser.reporter = reporter;
			parser.scope = scope;
			parser.rooibos();

		} catch (IOException e) {
			e.printStackTrace();
		}

		scope.leave(unit);
		
		SemanticChecker semanticChecker = new SemanticChecker(scope, reporter);
		unit.visit(semanticChecker);	

		TypeChecker typeChecker = new TypeChecker(scope, reporter);
		unit.visit(typeChecker);

		unit.printTree("|");

		CPPGenerator generator = new CPPGenerator(scope, reporter, "tests/out/");
		generator.generate(unit);

		reporter.print();
		System.out.println();
		
		int errorCount = reporter.getErrorCount();
		int warningCount = reporter.getWarningCount();
		
		if (errorCount > 0) {
			System.out.println("Failed: " + errorCount + " error(s), " + warningCount + " warning(s)");
		}
		else {
			System.out.println("Success: " + warningCount + " warning(s)");
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("\n" + 
				"Usage: rooibos <filename>\n" +
				"\n");
			return;
		}

		new Compiler(args[0]);
	}
}
