package whitesquare.rooibos.compiler.visitors;

import whitesquare.rooibos.compiler.ErrorReporter;
import whitesquare.rooibos.compiler.ScopeManager;
import whitesquare.rooibos.compiler.nodes.Assignment;
import whitesquare.rooibos.compiler.nodes.BinaryOp;
import whitesquare.rooibos.compiler.nodes.Call;
import whitesquare.rooibos.compiler.nodes.Constructor;
import whitesquare.rooibos.compiler.nodes.ConstructorCall;
import whitesquare.rooibos.compiler.nodes.Field;
import whitesquare.rooibos.compiler.nodes.Function;
import whitesquare.rooibos.compiler.nodes.FunctionCall;
import whitesquare.rooibos.compiler.nodes.IfStatement;
import whitesquare.rooibos.compiler.nodes.ScopeAccess;
import whitesquare.rooibos.compiler.nodes.Value;
import whitesquare.rooibos.compiler.nodes.Variable;
import whitesquare.rooibos.compiler.types.Type;

public class TypeChecker extends Visitor {
	public TypeChecker(ScopeManager scope, ErrorReporter reporter) {
		super(scope, reporter);
	}

	@Override
	public void visitAssignmentExit(Assignment assignment) {
		if (assignment.left == null || assignment.right == null) 
			return;

		if (assignment.left.type == Type.INVALID || 
				assignment.right.type == Type.INVALID) 
			return;

		if (assignment.left.isConst())
			reporter.error(assignment.source, "Can not assign a value to a constant");

		if (!assignment.left.type.isAssignableFrom(assignment.right.type))
			reporter.error(assignment.source, "Can not assign a value of type '" + 
				assignment.right.type + "' to a variable of type '" + assignment.left.type + "'");
	}

	@Override
	public void visitValue(Value value) {
		reporter.error(value.source, "visited a value (type checker) " + value + " " + value.type);
	}

	@Override
	public void visitBinaryOpExit(BinaryOp binaryOp) {
		if (binaryOp.left.type == Type.INVALID ||
				binaryOp.right.type == Type.INVALID) {
			binaryOp.type = Type.INVALID;
		}
		else if (binaryOp.left.type != binaryOp.right.type) {
			binaryOp.type = Type.INVALID;
			reporter.error(binaryOp.source, "Incompatible operands of types '" + 
				binaryOp.left.type + "' and '" + binaryOp.right.type + "' to operator '" + 
				binaryOp.op.symbol + "'");
		}
		else if (binaryOp.left.isConst() && binaryOp.right.isConst()) {
			binaryOp.setConst();
		}
		
		if (binaryOp.op == BinaryOp.Operation.EQUAL) {
			binaryOp.type = Type.BOOLEAN;
		}
	}

	@Override
	public void visitVariableExit(Variable variable) {
		if (variable.isConst() && variable.initial == null) {
			reporter.error(variable.source, "Constant '" + variable.name + "' must be initialized");
			return;
		}
	}
	
	@Override
	public void visitFieldExit(Field field) {
		if (field.isConst() && field.initial == null) {
			reporter.error(field.source, "Constant '" + field.name + "' must be initialized");
			return;
		}
	}	
	
	@Override
	public void visitIfAfterCond(IfStatement ifState) {
		if (ifState.condition.type != Type.BOOLEAN) {
			reporter.error(ifState.condition.source, "Condition must be a boolean");
		}
	}
	
	public void checkCall(Call call) {
		Function func = call.function;
		String name = "'" + func.name + "'";
		if (func instanceof Constructor) name = "the constructor of '" + ((Constructor)func).type.name + "'";
		
		String errStr = "";

		if (call.arguments.size() < func.parameters.size()) {
			errStr = "Too few arguments given to " + name + ", expected: ";
		}
		else if (call.arguments.size() > func.parameters.size()) {
			errStr = "Too many arguments given to " + name + ", expected: ";
		}
		else {
			for (int i = 0; i < func.parameters.size(); i++) {
				Variable parm = func.parameters.get(i);
				Value arg = call.arguments.get(i);

				if (!parm.type.isAssignableFrom(arg.type)) {
					reporter.error(arg.source, "Expected value of type '" + parm.type + "' but got '" + arg.type + "', for argument " + (i+1) + " of the call to " + name);
				}
			}

			return;
		}

		String parmStr = "";
		boolean first = true;
		for (Variable var : func.parameters) {
			if (first)
				first = false;
			else
				parmStr += ", ";
			parmStr += var.type.name + " " + var.name;
		}
		if (parmStr.isEmpty()) parmStr = "<nothing>";

		String argStr = "";
		first = true;
		for (Value arg : call.arguments) {
			if (first)
				first = false;
			else
				argStr += ", ";
			argStr += arg.type.name;
		}
		if (argStr.isEmpty()) argStr = "<nothing>";
		
		reporter.error(call.source, errStr + "\n | " + parmStr + "\ngot:\n | " + argStr);
	}
	
	@Override
	public void visitConstructorCallExit(ConstructorCall call) {
		checkCall(call);
	}

	@Override
	public void visitFunctionCallExit(FunctionCall call) {
		checkCall(call);
	}

	@Override
	public void visitScopeAccessExit(ScopeAccess scopeAccess) {
		scopeAccess.type = scopeAccess.value.type;
	}
}