package whitesquare.rooibos.compiler.visitors;

import whitesquare.rooibos.compiler.ErrorReporter;
import whitesquare.rooibos.compiler.ScopeManager;
import whitesquare.rooibos.compiler.nodes.Assignment;
import whitesquare.rooibos.compiler.nodes.BinaryOp;
import whitesquare.rooibos.compiler.nodes.Class;
import whitesquare.rooibos.compiler.nodes.Constructor;
import whitesquare.rooibos.compiler.nodes.ConstructorCall;
import whitesquare.rooibos.compiler.nodes.Field;
import whitesquare.rooibos.compiler.nodes.Function;
import whitesquare.rooibos.compiler.nodes.FunctionCall;
import whitesquare.rooibos.compiler.nodes.IfStatement;
import whitesquare.rooibos.compiler.nodes.Literal;
import whitesquare.rooibos.compiler.nodes.Method;
import whitesquare.rooibos.compiler.nodes.MethodCall;
import whitesquare.rooibos.compiler.nodes.Parameter;
import whitesquare.rooibos.compiler.nodes.Return;
import whitesquare.rooibos.compiler.nodes.ScopeAccess;
import whitesquare.rooibos.compiler.nodes.Statement;
import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;
import whitesquare.rooibos.compiler.nodes.Unit;
import whitesquare.rooibos.compiler.nodes.Value;
import whitesquare.rooibos.compiler.nodes.Variable;
import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
import whitesquare.rooibos.compiler.nodes.VariableValue;

public abstract class Visitor {
	protected ScopeManager scope;
	protected ErrorReporter reporter;

	public Visitor(ScopeManager scope, ErrorReporter reporter) {
		this.scope = scope;
		this.reporter = reporter;
	}

	public void visitUnitEnter(Unit unit) {}
	public void visitUnitExit(Unit unit) {}
	public void visitValue(Value value) {}
	public void visitLiteral(Literal literal) {}
	public void visitParameter(Parameter parameter) {}
	public void visitVariableEnter(Variable variable) {}
	public void visitVariableExit(Variable variable) {}
	public void visitFieldEnter(Field field) {}
	public void visitFieldExit(Field field) {}
	public void visitVariableValue(VariableValue value) {}
	public void visitFunctionEnter(Function function) {}
	public void visitFunctionExit(Function function) {}
	public void visitMethodEnter(Method method) {}
	public void visitMethodExit(Method method) {}
	public void visitConstructorEnter(Constructor constructor) {}
	public void visitConstructorExit(Constructor constructor) {}
	public void visitFunctionCallEnter(FunctionCall call) {}
	public void visitFunctionCallBetweenArg(FunctionCall call) {}
	public void visitFunctionCallExit(FunctionCall call) {}
	public void visitConstructorCallEnter(ConstructorCall call) {}
	public void visitConstructorCallBetweenArg(ConstructorCall call) {}
	public void visitConstructorCallExit(ConstructorCall call) {}
	public void visitMethodCallEnter(MethodCall call) {}
	public void visitMethodCallBetweenArg(MethodCall call) {}
	public void visitMethodCallExit(MethodCall call) {}
	public void visitIfBeforeCond(IfStatement ifStat) {}
	public void visitIfAfterCond(IfStatement ifStat) {}
	public void visitIfBeforeElse(IfStatement ifStat) {}
	public void visitIfExit(IfStatement ifStat) {}
	public void visitAssignmentEnter(Assignment assignment) {}
	public void visitAssignmentMid(Assignment assignment) {}
	public void visitAssignmentExit(Assignment assignment) {}
	public void visitReturnEnter(Return ret) {}
	public void visitReturnExit(Return ret) {}
	public void visitStatementEnter(Statement stat) {}
	public void visitStatementExit(Statement stat) {}
	public void visitBinaryOpEnter(BinaryOp binaryOp) {}
	public void visitBinaryOpMid(BinaryOp binaryOp) {}
	public void visitBinaryOpExit(BinaryOp binaryOp) {}
	public void visitClassEnter(Class klass) {}
	public void visitClassExit(Class klass) {}
	public void visitScopeAccessEnter(ScopeAccess scopeAccess) {}
	public void visitScopeAccessExit(ScopeAccess scopeAccess) {}
	public void visitVariableScopeAccessEnter(VariableScopeAccess scopeAccess) {}
	public void visitVariableScopeAccessExit(VariableScopeAccess scopeAccess) {}
	public void visitTypeAliasStatement(TypeAliasStatement typeAliasStatement) {}
}