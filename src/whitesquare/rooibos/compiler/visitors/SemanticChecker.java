package whitesquare.rooibos.compiler.visitors;

import java.util.LinkedList;

import whitesquare.rooibos.compiler.ErrorReporter;
import whitesquare.rooibos.compiler.ScopeManager;
import whitesquare.rooibos.compiler.nodes.ConstructorCall;
import whitesquare.rooibos.compiler.nodes.MethodCall;
import whitesquare.rooibos.compiler.nodes.Node;
import whitesquare.rooibos.compiler.nodes.Scope;
import whitesquare.rooibos.compiler.nodes.ScopeAccess;
import whitesquare.rooibos.compiler.nodes.Variable;
import whitesquare.rooibos.compiler.types.Modifier;
import whitesquare.rooibos.compiler.types.Type;

public class SemanticChecker extends Visitor {
	LinkedList<Node> nodeScopes = new LinkedList<Node>();
	
	public SemanticChecker(ScopeManager scope, ErrorReporter reporter) {
		super(scope, reporter);
	}

	@Override
	public void visitConstructorCallEnter(ConstructorCall call) {
		Node node = nodeScopes.peek();
		
		if (node instanceof Variable)
			reporter.error(call.source, "Constructors should only be called on the type itself and not on a variable");
	}

	@Override
	public void visitMethodCallEnter(MethodCall call) {
		Node node = nodeScopes.peek();
		boolean isStatic = call.function.modifiers.contains(Modifier.STATIC);
		boolean isScopeVariable = node instanceof Variable;
		
		if (isScopeVariable && isStatic)
			reporter.error(call.source, "Static methods should only be called on the class itself not on a variable");
		else if (!isScopeVariable && !isStatic)
			reporter.error(call.source, "Non-static methods must be called on an instance of the class");
	}

	@Override
	public void visitScopeAccessEnter(ScopeAccess scopeAccess) {
		Node nodeScope = scopeAccess.scope;
		nodeScopes.push(nodeScope);
		
		if (nodeScope instanceof Type) {
			scope.enter((Scope)nodeScope);
			return;
		}
		
		if (nodeScope instanceof Variable) {
			scope.enter(((Variable)nodeScope).type);
			return;
		}
		
		reporter.error(scopeAccess.source, "Can not use '" + nodeScope.name + "' as a scope");
	}	
	
	@Override
	public void visitScopeAccessExit(ScopeAccess scopeAccess) {
		scope.leave();
		nodeScopes.pop();
	}
}
