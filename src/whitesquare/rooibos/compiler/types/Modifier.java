package whitesquare.rooibos.compiler.types;

public enum Modifier {
	SHARED, CONST, STATIC
}