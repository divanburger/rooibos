package whitesquare.rooibos.compiler.types;

import whitesquare.rooibos.compiler.nodes.Constructor;
import whitesquare.rooibos.compiler.nodes.Node;
import whitesquare.rooibos.compiler.nodes.Scope;
import whitesquare.rooibos.compiler.nodes.Source;

public class Type extends Scope implements Comparable<Type> {
	public String realName = null;

	public Constructor constructor = null;
	public Type superType = TOP;
	
	static public Type INVALID = new Type("invalid");
	static public Type BOOLEAN = new Type("bool");
	static public Type TOP = new Type("Anything");
	static public Type BOTTOM = new Type("Nothing");

	public Type(String name) {
		super(Source.UNKNOWN, name);
	}
	
	public Type(Source source, String name) {
		super(source, name);
	}
	
	public Type(Type type) {
		super(Source.UNKNOWN, type.name);
	}
	
	public void finish() {
		if (constructor != null) return;
		
		if (superType != null) {
			Constructor superConstr = superType.findConstructor();
			constructor = superConstr.copyWithSignature(this);
		}
		else
			constructor = new Constructor(Source.GENERATED, this);
	}
	
	public Constructor findConstructor() {
		if (constructor != null) return constructor;
		if (superType != null) return superType.findConstructor();
		throw new Error("Class '" + this.name + "' does not have a constructor");
	}
		
	public String getRealName() {
		return realName == null ? name : realName;
	}
	
	public boolean isAssignableFrom(Type type) {
		return (this == type);
	}
	
	public Type union(Type other) {
		if (other instanceof UnionType) {
			UnionType res = (UnionType)other.clone();
			res.addType(this);
			return res;
		}
		if (this == other) return this;
		return new UnionType(this, other);
	}
	
	public Node get(String name) {
		if (this == TOP) return null;
		
		for (Node node : children)
			if (node.name.equals(name))
				return node;
		if (superType != null) return superType.get(name);
		return null;	
	}
	
	public Type clone() {
		return new Type(this);
	}

	@Override
	public String toString() {
		String str = name;
		if (superType != null) str += " [extends " + superType.name + "]";
		return str;
	}
	
	@Override
	public int compareTo(Type other) {
		return name.compareTo(other.name);
	}
}