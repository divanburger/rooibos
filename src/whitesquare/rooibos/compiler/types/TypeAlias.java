package whitesquare.rooibos.compiler.types;

import whitesquare.rooibos.compiler.nodes.Node;
import whitesquare.rooibos.compiler.nodes.Source;

public class TypeAlias extends Type {
	public Type aliasOf;

	public TypeAlias(Source source, String name, Type type) {
		super(source, name);
		aliasOf = type;
	}
	
	@Override
	public boolean isAssignableFrom(Type type) {
		return aliasOf.isAssignableFrom(type);
	}
	
	@Override
	public Node get(String name) {
		return aliasOf.get(name);
	}
	
	@Override
	public String toString() {
		return name + " (aka " + aliasOf.toString() + ")";
	}
}
