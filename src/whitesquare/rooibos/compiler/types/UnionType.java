package whitesquare.rooibos.compiler.types;

import java.util.Set;
import java.util.TreeSet;

import whitesquare.rooibos.compiler.nodes.Node;

public class UnionType extends Type {
	
	public Set<Type> types = new TreeSet<Type>();
	
	public UnionType(String name) {
		super(name);
	}
	
	public UnionType(UnionType other) {
		super(other.name);
		types.addAll(other.types);
	}	
	
	public UnionType(Type type1, Type type2) {
		super(type1 + "|" + type2);
		types.add(type1);
		types.add(type2);
	}
	
	public UnionType(Type type1, Type type2, Type type3) {
		super(type1 + "|" + type2 + "|" + type3);
		types.add(type1);
		types.add(type2);
		types.add(type3);
	}
	
	public UnionType(Type type1, Type type2, Type type3, Type type4) {
		super(type1 + "|" + type2 + "|" + type3 + "|" + type4);
		types.add(type1);
		types.add(type2);
		types.add(type3);
		types.add(type4);
	}
	
	public void addType(Type type) {
		types.add(type);
	}
	
	@Override
	public Type union(Type other) {
		UnionType res = new UnionType(this);
		if (other instanceof UnionType)
			res.types.addAll(((UnionType)other).types);
		else
			res.addType(other);
		if (res.types.size() == 1) return (Type)res.types.toArray()[0];
		return res;
	}
	
	public Type getCommonType() {
		Type common = null;
		for (Type cur : types) {
			if (common == null)
				common = cur;
			else {
				common = getCommonType(cur, common);
			}
		}
		
		return common;
	}
	
	private Type getCommonType(Type first, Type second) {
		if (first == second) return first;
		if (first == Type.TOP || second == Type.TOP) return Type.TOP;
		
		Type secondCur = second;
		
		while (secondCur != Type.TOP) {
			Type firstCur = first;
			
			while (firstCur != Type.TOP) {
				if (firstCur == secondCur) return firstCur;
				
				firstCur = firstCur.superType;
			}
			
			secondCur = secondCur.superType;
		}
		
		return Type.TOP;
	}	
	
	@Override
	public Node get(String name) {
		if (types.isEmpty()) return null;
		
		Node node = null;
		for (Type type : types) {
			Node cur = type.get(name);
			if (cur == null) return null;
			if (node != null && !cur.isEquivalent(node)) return null;
			node = cur;
		}
		
		return node;
	}

	@Override
	public Type clone() {
		return new UnionType(this);
	}
	
	@Override
	public boolean isAssignableFrom(Type type) {
		if (type instanceof UnionType) {
			UnionType utype = (UnionType)type;
			for (Type other : utype.types) {
				if (!types.contains(other)) return false;
			}
			return true;
		}
		return types.contains(type);
	}

	@Override
	public String getDisplayName() {
		String str = "";
		boolean first = true;
		for (Type t : types) {
			if (first)
				first = false;
			else
				str += "|";
			str += t.getDisplayName();
		}
		return str;
	}
	
	@Override
	public String toString() {
		String str = "";
		boolean first = true;
		for (Type t : types) {
			if (first)
				first = false;
			else
				str += "|";
			str += t.getDisplayName();
		}
		return str;
	}
}
