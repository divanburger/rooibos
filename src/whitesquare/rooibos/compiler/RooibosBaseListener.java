// Generated from grammar/Rooibos.g4 by ANTLR 4.0
package whitesquare.rooibos.compiler;

	import java.util.LinkedList;
	import java.util.Set;
	import java.util.TreeSet;

	import whitesquare.rooibos.compiler.nodes.Node;
	import whitesquare.rooibos.compiler.nodes.Scope;
	import whitesquare.rooibos.compiler.nodes.Constructor;
	import whitesquare.rooibos.compiler.nodes.Method;
	import whitesquare.rooibos.compiler.nodes.Function;
	import whitesquare.rooibos.compiler.nodes.Call;
	import whitesquare.rooibos.compiler.nodes.ConstructorCall;
	import whitesquare.rooibos.compiler.nodes.MethodCall;
	import whitesquare.rooibos.compiler.nodes.FunctionCall;
	import whitesquare.rooibos.compiler.nodes.Assignment;
	import whitesquare.rooibos.compiler.nodes.Statement;
	import whitesquare.rooibos.compiler.nodes.Return;
	import whitesquare.rooibos.compiler.nodes.Parameter;
	import whitesquare.rooibos.compiler.nodes.Field;
	import whitesquare.rooibos.compiler.nodes.ScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Variable;
	import whitesquare.rooibos.compiler.nodes.VariableValue;
	import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Value;
	import whitesquare.rooibos.compiler.nodes.Literal;
	import whitesquare.rooibos.compiler.nodes.BinaryOp;
	import whitesquare.rooibos.compiler.nodes.Source;
	import whitesquare.rooibos.compiler.nodes.IfStatement;
	import whitesquare.rooibos.compiler.nodes.Class;
	import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;

	import whitesquare.rooibos.compiler.types.Modifier;
	import whitesquare.rooibos.compiler.types.Type;
	import whitesquare.rooibos.compiler.types.TypeAlias;
	import whitesquare.rooibos.compiler.types.UnionType;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class RooibosBaseListener implements RooibosListener {
	@Override public void enterVarPart(RooibosParser.VarPartContext ctx) { }
	@Override public void exitVarPart(RooibosParser.VarPartContext ctx) { }

	@Override public void enterExpression(RooibosParser.ExpressionContext ctx) { }
	@Override public void exitExpression(RooibosParser.ExpressionContext ctx) { }

	@Override public void enterCall(RooibosParser.CallContext ctx) { }
	@Override public void exitCall(RooibosParser.CallContext ctx) { }

	@Override public void enterAtom(RooibosParser.AtomContext ctx) { }
	@Override public void exitAtom(RooibosParser.AtomContext ctx) { }

	@Override public void enterCallStatement(RooibosParser.CallStatementContext ctx) { }
	@Override public void exitCallStatement(RooibosParser.CallStatementContext ctx) { }

	@Override public void enterVariableValue(RooibosParser.VariableValueContext ctx) { }
	@Override public void exitVariableValue(RooibosParser.VariableValueContext ctx) { }

	@Override public void enterReturnStatement(RooibosParser.ReturnStatementContext ctx) { }
	@Override public void exitReturnStatement(RooibosParser.ReturnStatementContext ctx) { }

	@Override public void enterBlock(RooibosParser.BlockContext ctx) { }
	@Override public void exitBlock(RooibosParser.BlockContext ctx) { }

	@Override public void enterType(RooibosParser.TypeContext ctx) { }
	@Override public void exitType(RooibosParser.TypeContext ctx) { }

	@Override public void enterTypeAtom(RooibosParser.TypeAtomContext ctx) { }
	@Override public void exitTypeAtom(RooibosParser.TypeAtomContext ctx) { }

	@Override public void enterTypeAlias(RooibosParser.TypeAliasContext ctx) { }
	@Override public void exitTypeAlias(RooibosParser.TypeAliasContext ctx) { }

	@Override public void enterParameter(RooibosParser.ParameterContext ctx) { }
	@Override public void exitParameter(RooibosParser.ParameterContext ctx) { }

	@Override public void enterFieldDeclaration(RooibosParser.FieldDeclarationContext ctx) { }
	@Override public void exitFieldDeclaration(RooibosParser.FieldDeclarationContext ctx) { }

	@Override public void enterStatements(RooibosParser.StatementsContext ctx) { }
	@Override public void exitStatements(RooibosParser.StatementsContext ctx) { }

	@Override public void enterEol(RooibosParser.EolContext ctx) { }
	@Override public void exitEol(RooibosParser.EolContext ctx) { }

	@Override public void enterFieldPart(RooibosParser.FieldPartContext ctx) { }
	@Override public void exitFieldPart(RooibosParser.FieldPartContext ctx) { }

	@Override public void enterModifiers(RooibosParser.ModifiersContext ctx) { }
	@Override public void exitModifiers(RooibosParser.ModifiersContext ctx) { }

	@Override public void enterMethodDeclaration(RooibosParser.MethodDeclarationContext ctx) { }
	@Override public void exitMethodDeclaration(RooibosParser.MethodDeclarationContext ctx) { }

	@Override public void enterVarDeclaration(RooibosParser.VarDeclarationContext ctx) { }
	@Override public void exitVarDeclaration(RooibosParser.VarDeclarationContext ctx) { }

	@Override public void enterArgument(RooibosParser.ArgumentContext ctx) { }
	@Override public void exitArgument(RooibosParser.ArgumentContext ctx) { }

	@Override public void enterScoping(RooibosParser.ScopingContext ctx) { }
	@Override public void exitScoping(RooibosParser.ScopingContext ctx) { }

	@Override public void enterConstructorDeclaration(RooibosParser.ConstructorDeclarationContext ctx) { }
	@Override public void exitConstructorDeclaration(RooibosParser.ConstructorDeclarationContext ctx) { }

	@Override public void enterClassStatement(RooibosParser.ClassStatementContext ctx) { }
	@Override public void exitClassStatement(RooibosParser.ClassStatementContext ctx) { }

	@Override public void enterCondition(RooibosParser.ConditionContext ctx) { }
	@Override public void exitCondition(RooibosParser.ConditionContext ctx) { }

	@Override public void enterClassDeclaration(RooibosParser.ClassDeclarationContext ctx) { }
	@Override public void exitClassDeclaration(RooibosParser.ClassDeclarationContext ctx) { }

	@Override public void enterIfStatement(RooibosParser.IfStatementContext ctx) { }
	@Override public void exitIfStatement(RooibosParser.IfStatementContext ctx) { }

	@Override public void enterStatement(RooibosParser.StatementContext ctx) { }
	@Override public void exitStatement(RooibosParser.StatementContext ctx) { }

	@Override public void enterModifier(RooibosParser.ModifierContext ctx) { }
	@Override public void exitModifier(RooibosParser.ModifierContext ctx) { }

	@Override public void enterAssignment(RooibosParser.AssignmentContext ctx) { }
	@Override public void exitAssignment(RooibosParser.AssignmentContext ctx) { }

	@Override public void enterTerm(RooibosParser.TermContext ctx) { }
	@Override public void exitTerm(RooibosParser.TermContext ctx) { }

	@Override public void enterFunctionDeclaration(RooibosParser.FunctionDeclarationContext ctx) { }
	@Override public void exitFunctionDeclaration(RooibosParser.FunctionDeclarationContext ctx) { }

	@Override public void enterVariable(RooibosParser.VariableContext ctx) { }
	@Override public void exitVariable(RooibosParser.VariableContext ctx) { }

	@Override public void enterRooibos(RooibosParser.RooibosContext ctx) { }
	@Override public void exitRooibos(RooibosParser.RooibosContext ctx) { }

	@Override public void enterPolynomial(RooibosParser.PolynomialContext ctx) { }
	@Override public void exitPolynomial(RooibosParser.PolynomialContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}