package whitesquare.rooibos.compiler;

import java.util.Stack;

import java.io.Writer;
import java.io.FileWriter;
import java.io.IOException;

class CodeWriter {
	private Writer out;
	private Stack<String> indentationStack = new Stack<String>();
	private String indentation = "";
	private boolean skipLine = false;

	public CodeWriter(Writer output) {
		out = output;
	}

	public CodeWriter(String filename) {
		try {
			out = new FileWriter(filename);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void skipLine() {
		skipLine = true;
	}

	public void writeln(String line) {
		try {
			if (skipLine) out.write("\n");
			skipLine = false;
			out.write(indentation + line + "\n");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writelnStart(String start) {
		try {
			if (skipLine) out.write("\n");
			skipLine = false;
			out.write(indentation + start);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writelnMid(String mid) {
		try {
			out.write(mid);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}	

	public void writelnEnd(String end) {
		try {
			out.write(end + "\n");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void indent() {
		indentationStack.push("\t");
		indentation = "";
		for (String s : indentationStack)
			indentation += s;		
	}

	public void unindent() {
		skipLine = false;
		indentationStack.pop();
		indentation = "";
		for (String s : indentationStack)
			indentation += s;	
	}

	public void close() {
		try {
			out.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}