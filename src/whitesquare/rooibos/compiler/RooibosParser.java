// Generated from grammar/Rooibos.g4 by ANTLR 4.0
package whitesquare.rooibos.compiler;

	import java.util.LinkedList;
	import java.util.Set;
	import java.util.TreeSet;

	import whitesquare.rooibos.compiler.nodes.Node;
	import whitesquare.rooibos.compiler.nodes.Scope;
	import whitesquare.rooibos.compiler.nodes.Constructor;
	import whitesquare.rooibos.compiler.nodes.Method;
	import whitesquare.rooibos.compiler.nodes.Function;
	import whitesquare.rooibos.compiler.nodes.Call;
	import whitesquare.rooibos.compiler.nodes.ConstructorCall;
	import whitesquare.rooibos.compiler.nodes.MethodCall;
	import whitesquare.rooibos.compiler.nodes.FunctionCall;
	import whitesquare.rooibos.compiler.nodes.Assignment;
	import whitesquare.rooibos.compiler.nodes.Statement;
	import whitesquare.rooibos.compiler.nodes.Return;
	import whitesquare.rooibos.compiler.nodes.Parameter;
	import whitesquare.rooibos.compiler.nodes.Field;
	import whitesquare.rooibos.compiler.nodes.ScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Variable;
	import whitesquare.rooibos.compiler.nodes.VariableValue;
	import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Value;
	import whitesquare.rooibos.compiler.nodes.Literal;
	import whitesquare.rooibos.compiler.nodes.BinaryOp;
	import whitesquare.rooibos.compiler.nodes.Source;
	import whitesquare.rooibos.compiler.nodes.IfStatement;
	import whitesquare.rooibos.compiler.nodes.Class;
	import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;

	import whitesquare.rooibos.compiler.types.Modifier;
	import whitesquare.rooibos.compiler.types.Type;
	import whitesquare.rooibos.compiler.types.TypeAlias;
	import whitesquare.rooibos.compiler.types.UnionType;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RooibosParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__25=1, T__24=2, T__23=3, T__22=4, T__21=5, T__20=6, T__19=7, T__18=8, 
		T__17=9, T__16=10, T__15=11, T__14=12, T__13=13, T__12=14, T__11=15, T__10=16, 
		T__9=17, T__8=18, T__7=19, T__6=20, T__5=21, T__4=22, T__3=23, T__2=24, 
		T__1=25, T__0=26, Id=27, Integer=28, Float=29, String=30, WS=31;
	public static final String[] tokenNames = {
		"<INVALID>", "','", "'*'", "'-'", "'('", "':'", "'if'", "'var'", "'{'", 
		"'method'", "'extends'", "'constructor'", "'}'", "'static'", "'function'", 
		"')'", "'.'", "'+'", "'return'", "'='", "';'", "'type'", "'val'", "'/'", 
		"'=='", "'class'", "'|'", "Id", "Integer", "Float", "String", "WS"
	};
	public static final int
		RULE_rooibos = 0, RULE_block = 1, RULE_statements = 2, RULE_statement = 3, 
		RULE_classDeclaration = 4, RULE_typeAlias = 5, RULE_classStatement = 6, 
		RULE_constructorDeclaration = 7, RULE_methodDeclaration = 8, RULE_functionDeclaration = 9, 
		RULE_parameter = 10, RULE_fieldDeclaration = 11, RULE_varDeclaration = 12, 
		RULE_fieldPart = 13, RULE_varPart = 14, RULE_ifStatement = 15, RULE_assignment = 16, 
		RULE_returnStatement = 17, RULE_modifiers = 18, RULE_modifier = 19, RULE_condition = 20, 
		RULE_expression = 21, RULE_polynomial = 22, RULE_term = 23, RULE_atom = 24, 
		RULE_callStatement = 25, RULE_call = 26, RULE_argument = 27, RULE_variable = 28, 
		RULE_variableValue = 29, RULE_type = 30, RULE_typeAtom = 31, RULE_scoping = 32, 
		RULE_eol = 33;
	public static final String[] ruleNames = {
		"rooibos", "block", "statements", "statement", "classDeclaration", "typeAlias", 
		"classStatement", "constructorDeclaration", "methodDeclaration", "functionDeclaration", 
		"parameter", "fieldDeclaration", "varDeclaration", "fieldPart", "varPart", 
		"ifStatement", "assignment", "returnStatement", "modifiers", "modifier", 
		"condition", "expression", "polynomial", "term", "atom", "callStatement", 
		"call", "argument", "variable", "variableValue", "type", "typeAtom", "scoping", 
		"eol"
	};

	@Override
	public String getGrammarFileName() { return "Rooibos.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }


		public ErrorReporter reporter;
		public ScopeManager scope;
		
		private Value binaryOp(Token token, BinaryOp.Operation op, Value left, Value right) {
			if (left == null || right == null) {
				System.out.println(op.name());
				return new Value(new Source(token), Type.INVALID);
			}
			return new BinaryOp(new Source(token), op, left, right);
		}

	public RooibosParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RooibosContext extends ParserRuleContext {
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public TerminalNode EOF() { return getToken(RooibosParser.EOF, 0); }
		public RooibosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rooibos; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterRooibos(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitRooibos(this);
		}
	}

	public final RooibosContext rooibos() throws RecognitionException {
		RooibosContext _localctx = new RooibosContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_rooibos);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); statements();
			setState(69); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public StatementsContext statements() {
			return getRuleContext(StatementsContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71); match(8);
			setState(72); statements();
			setState(73); match(12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementsContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitStatements(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_statements);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 6) | (1L << 7) | (1L << 14) | (1L << 18) | (1L << 21) | (1L << 22) | (1L << 25) | (1L << Id))) != 0)) {
				{
				{
				setState(75); statement();
				}
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public TypeAliasContext typeAlias() {
			return getRuleContext(TypeAliasContext.class,0);
		}
		public CallStatementContext callStatement() {
			return getRuleContext(CallStatementContext.class,0);
		}
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public ReturnStatementContext returnStatement() {
			return getRuleContext(ReturnStatementContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public FunctionDeclarationContext functionDeclaration() {
			return getRuleContext(FunctionDeclarationContext.class,0);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public VarDeclarationContext varDeclaration() {
			return getRuleContext(VarDeclarationContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_statement);
		try {
			setState(90);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(81); match(Id);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(82); functionDeclaration();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(83); varDeclaration();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(84); assignment();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(85); returnStatement();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(86); callStatement();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(87); ifStatement();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(88); classDeclaration();
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(89); typeAlias();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public Token Id;
		public TypeAtomContext typeAtom;
		public TypeAtomContext typeAtom() {
			return getRuleContext(TypeAtomContext.class,0);
		}
		public ConstructorDeclarationContext constructorDeclaration() {
			return getRuleContext(ConstructorDeclarationContext.class,0);
		}
		public List<ClassStatementContext> classStatement() {
			return getRuleContexts(ClassStatementContext.class);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public ClassStatementContext classStatement(int i) {
			return getRuleContext(ClassStatementContext.class,i);
		}
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitClassDeclaration(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_classDeclaration);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(92); match(25);
			setState(93); ((ClassDeclarationContext)_localctx).Id = match(Id);

					Class klass = new Class(new Source(((ClassDeclarationContext)_localctx).Id), (((ClassDeclarationContext)_localctx).Id!=null?((ClassDeclarationContext)_localctx).Id.getText():null));
				
			setState(99);
			_la = _input.LA(1);
			if (_la==10) {
				{
				setState(95); match(10);
				setState(96); ((ClassDeclarationContext)_localctx).typeAtom = typeAtom();

							if (((ClassDeclarationContext)_localctx).typeAtom.t != null) klass.superType = ((ClassDeclarationContext)_localctx).typeAtom.t;
						
				}
			}


					scope.add(klass);
					scope.enter(klass);
				
			setState(102); match(8);
			setState(106);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(103); classStatement(klass);
					}
					} 
				}
				setState(108);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			setState(110);
			_la = _input.LA(1);
			if (_la==11) {
				{
				setState(109); constructorDeclaration(klass);
				}
			}

			setState(115);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 7) | (1L << 9) | (1L << 13) | (1L << 22))) != 0)) {
				{
				{
				setState(112); classStatement(klass);
				}
				}
				setState(117);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(118); match(12);

					scope.leave(klass);
					klass.finish();
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeAliasContext extends ParserRuleContext {
		public Token Id;
		public TypeContext type;
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeAlias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterTypeAlias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitTypeAlias(this);
		}
	}

	public final TypeAliasContext typeAlias() throws RecognitionException {
		TypeAliasContext _localctx = new TypeAliasContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_typeAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(121); match(21);
			setState(122); ((TypeAliasContext)_localctx).Id = match(Id);
			setState(123); match(19);
			setState(124); ((TypeAliasContext)_localctx).type = type();

					TypeAlias alias = new TypeAlias(new Source(((TypeAliasContext)_localctx).Id), (((TypeAliasContext)_localctx).Id!=null?((TypeAliasContext)_localctx).Id.getText():null), ((TypeAliasContext)_localctx).type.t);
					scope.add(alias);
					
					TypeAliasStatement aliasStatement = new TypeAliasStatement(new Source(((TypeAliasContext)_localctx).Id), alias);
					scope.add(aliasStatement);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassStatementContext extends ParserRuleContext {
		public Class klass;
		public FieldDeclarationContext fieldDeclaration() {
			return getRuleContext(FieldDeclarationContext.class,0);
		}
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public ClassStatementContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ClassStatementContext(ParserRuleContext parent, int invokingState, Class klass) {
			super(parent, invokingState);
			this.klass = klass;
		}
		@Override public int getRuleIndex() { return RULE_classStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterClassStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitClassStatement(this);
		}
	}

	public final ClassStatementContext classStatement(Class klass) throws RecognitionException {
		ClassStatementContext _localctx = new ClassStatementContext(_ctx, getState(), klass);
		enterRule(_localctx, 12, RULE_classStatement);
		try {
			setState(129);
			switch (_input.LA(1)) {
			case 9:
			case 13:
				enterOuterAlt(_localctx, 1);
				{
				setState(127); methodDeclaration(klass);
				}
				break;
			case 7:
			case 22:
				enterOuterAlt(_localctx, 2);
				{
				setState(128); fieldDeclaration(klass);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorDeclarationContext extends ParserRuleContext {
		public Class klass;
		public Constructor constructor;
		public Token f;
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ConstructorDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ConstructorDeclarationContext(ParserRuleContext parent, int invokingState, Class klass) {
			super(parent, invokingState);
			this.klass = klass;
		}
		@Override public int getRuleIndex() { return RULE_constructorDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterConstructorDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitConstructorDeclaration(this);
		}
	}

	public final ConstructorDeclarationContext constructorDeclaration(Class klass) throws RecognitionException {
		ConstructorDeclarationContext _localctx = new ConstructorDeclarationContext(_ctx, getState(), klass);
		enterRule(_localctx, 14, RULE_constructorDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131); ((ConstructorDeclarationContext)_localctx).f = match(11);

					Constructor constr = new Constructor(new Source(((ConstructorDeclarationContext)_localctx).f), _localctx.klass);
					klass.constructor = constr;
					scope.enter(constr);
				
			setState(133); match(4);
			setState(142);
			_la = _input.LA(1);
			if (_la==Id) {
				{
				setState(134); parameter(constr);
				setState(139);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==1) {
					{
					{
					setState(135); match(1);
					setState(136); parameter(constr);
					}
					}
					setState(141);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(144); match(15);
			setState(145); block();

					scope.leave(constr);
					((ConstructorDeclarationContext)_localctx).constructor =  constr;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public Class klass;
		public Method method;
		public ModifiersContext modifiers;
		public Token f;
		public Token Id;
		public Token rt;
		public TypeContext type;
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ModifiersContext modifiers() {
			return getRuleContext(ModifiersContext.class,0);
		}
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState, Class klass) {
			super(parent, invokingState);
			this.klass = klass;
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitMethodDeclaration(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration(Class klass) throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState(), klass);
		enterRule(_localctx, 16, RULE_methodDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(148); ((MethodDeclarationContext)_localctx).modifiers = modifiers();
			setState(149); ((MethodDeclarationContext)_localctx).f = match(9);
			setState(150); ((MethodDeclarationContext)_localctx).Id = match(Id);

					Method meth = new Method(new Source(((MethodDeclarationContext)_localctx).f), (((MethodDeclarationContext)_localctx).Id!=null?((MethodDeclarationContext)_localctx).Id.getText():null), _localctx.klass);
					meth.modifiers.addAll(((MethodDeclarationContext)_localctx).modifiers.mods);		
					scope.enter(meth);
				
			setState(152); match(4);
			setState(161);
			_la = _input.LA(1);
			if (_la==Id) {
				{
				setState(153); parameter(meth);
				setState(158);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==1) {
					{
					{
					setState(154); match(1);
					setState(155); parameter(meth);
					}
					}
					setState(160);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(163); match(15);
			setState(166);
			_la = _input.LA(1);
			if (_la==5) {
				{
				setState(164); ((MethodDeclarationContext)_localctx).rt = match(5);
				setState(165); ((MethodDeclarationContext)_localctx).type = type();
				}
			}

			setState(168); block();

					meth.returnType = ((MethodDeclarationContext)_localctx).rt != null ? ((MethodDeclarationContext)_localctx).type.t : Type.TOP;
					scope.leave(meth);
					((MethodDeclarationContext)_localctx).method =  meth;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclarationContext extends ParserRuleContext {
		public Function function;
		public Token f;
		public Token Id;
		public Token rt;
		public TypeContext type;
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FunctionDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterFunctionDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitFunctionDeclaration(this);
		}
	}

	public final FunctionDeclarationContext functionDeclaration() throws RecognitionException {
		FunctionDeclarationContext _localctx = new FunctionDeclarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_functionDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(171); ((FunctionDeclarationContext)_localctx).f = match(14);
			setState(172); ((FunctionDeclarationContext)_localctx).Id = match(Id);

					Function func = new Function(new Source(((FunctionDeclarationContext)_localctx).f), (((FunctionDeclarationContext)_localctx).Id!=null?((FunctionDeclarationContext)_localctx).Id.getText():null));
					scope.enter(func);
				
			setState(174); match(4);
			setState(183);
			_la = _input.LA(1);
			if (_la==Id) {
				{
				setState(175); parameter(func);
				setState(180);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==1) {
					{
					{
					setState(176); match(1);
					setState(177); parameter(func);
					}
					}
					setState(182);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(185); match(15);
			setState(188);
			_la = _input.LA(1);
			if (_la==5) {
				{
				setState(186); ((FunctionDeclarationContext)_localctx).rt = match(5);
				setState(187); ((FunctionDeclarationContext)_localctx).type = type();
				}
			}

			setState(190); block();

					func.returnType = ((FunctionDeclarationContext)_localctx).rt != null ? ((FunctionDeclarationContext)_localctx).type.t : Type.TOP;
					scope.leave(func);
					((FunctionDeclarationContext)_localctx).function =  func;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Function func;
		public Token Id;
		public TypeContext type;
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ParameterContext(ParserRuleContext parent, int invokingState, Function func) {
			super(parent, invokingState);
			this.func = func;
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter(Function func) throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState(), func);
		enterRule(_localctx, 20, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(193); ((ParameterContext)_localctx).Id = match(Id);
			setState(194); match(5);
			setState(195); ((ParameterContext)_localctx).type = type();

					Parameter parameter = new Parameter(new Source((((ParameterContext)_localctx).Id!=null?((ParameterContext)_localctx).Id.getText():null)), ((ParameterContext)_localctx).type.t, (((ParameterContext)_localctx).Id!=null?((ParameterContext)_localctx).Id.getText():null));
					func.addParameter(parameter);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDeclarationContext extends ParserRuleContext {
		public Class klass;
		public FieldPartContext fieldPart;
		public List<FieldPartContext> fieldParts = new ArrayList<FieldPartContext>();
		public TypeContext type;
		public FieldPartContext fieldPart(int i) {
			return getRuleContext(FieldPartContext.class,i);
		}
		public EolContext eol() {
			return getRuleContext(EolContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<FieldPartContext> fieldPart() {
			return getRuleContexts(FieldPartContext.class);
		}
		public FieldDeclarationContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public FieldDeclarationContext(ParserRuleContext parent, int invokingState, Class klass) {
			super(parent, invokingState);
			this.klass = klass;
		}
		@Override public int getRuleIndex() { return RULE_fieldDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterFieldDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitFieldDeclaration(this);
		}
	}

	public final FieldDeclarationContext fieldDeclaration(Class klass) throws RecognitionException {
		FieldDeclarationContext _localctx = new FieldDeclarationContext(_ctx, getState(), klass);
		enterRule(_localctx, 22, RULE_fieldDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			boolean isConst = false;
			setState(202);
			switch (_input.LA(1)) {
			case 7:
				{
				setState(199); match(7);
				}
				break;
			case 22:
				{
				setState(200); match(22);
				isConst = true;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(204); ((FieldDeclarationContext)_localctx).fieldPart = fieldPart();
			((FieldDeclarationContext)_localctx).fieldParts.add(((FieldDeclarationContext)_localctx).fieldPart);
			setState(209);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==1) {
				{
				{
				setState(205); match(1);
				setState(206); ((FieldDeclarationContext)_localctx).fieldPart = fieldPart();
				((FieldDeclarationContext)_localctx).fieldParts.add(((FieldDeclarationContext)_localctx).fieldPart);
				}
				}
				setState(211);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(216);
			_la = _input.LA(1);
			if (_la==5) {
				{
				setState(212); match(5);
				setState(213); ((FieldDeclarationContext)_localctx).type = type();

							for (FieldPartContext fieldPart : ((FieldDeclarationContext)_localctx).fieldParts) fieldPart.fld.type = ((FieldDeclarationContext)_localctx).type.t;
						
				}
			}

			setState(218); eol();

					for (FieldPartContext fieldPart : ((FieldDeclarationContext)_localctx).fieldParts) {
						Field field = fieldPart.fld;
						if (isConst) field.setConst();
						scope.add(field);
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarDeclarationContext extends ParserRuleContext {
		public VarPartContext varPart;
		public List<VarPartContext> varParts = new ArrayList<VarPartContext>();
		public TypeContext type;
		public List<VarPartContext> varPart() {
			return getRuleContexts(VarPartContext.class);
		}
		public EolContext eol() {
			return getRuleContext(EolContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VarPartContext varPart(int i) {
			return getRuleContext(VarPartContext.class,i);
		}
		public VarDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterVarDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitVarDeclaration(this);
		}
	}

	public final VarDeclarationContext varDeclaration() throws RecognitionException {
		VarDeclarationContext _localctx = new VarDeclarationContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_varDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			boolean isConst = false;
			setState(225);
			switch (_input.LA(1)) {
			case 7:
				{
				setState(222); match(7);
				}
				break;
			case 22:
				{
				setState(223); match(22);
				isConst = true;
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(227); ((VarDeclarationContext)_localctx).varPart = varPart();
			((VarDeclarationContext)_localctx).varParts.add(((VarDeclarationContext)_localctx).varPart);
			setState(232);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==1) {
				{
				{
				setState(228); match(1);
				setState(229); ((VarDeclarationContext)_localctx).varPart = varPart();
				((VarDeclarationContext)_localctx).varParts.add(((VarDeclarationContext)_localctx).varPart);
				}
				}
				setState(234);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(239);
			_la = _input.LA(1);
			if (_la==5) {
				{
				setState(235); match(5);
				setState(236); ((VarDeclarationContext)_localctx).type = type();

							for (VarPartContext varPart : ((VarDeclarationContext)_localctx).varParts) varPart.var.type = ((VarDeclarationContext)_localctx).type.t;
						
				}
			}

			setState(242);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				{
				setState(241); eol();
				}
				break;
			}

					for (VarPartContext varPart : ((VarDeclarationContext)_localctx).varParts) {
						Variable var = varPart.var;
						if (isConst) var.setConst();
						scope.add(var);
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldPartContext extends ParserRuleContext {
		public Field fld;
		public Token Id;
		public Token e;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public FieldPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldPart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterFieldPart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitFieldPart(this);
		}
	}

	public final FieldPartContext fieldPart() throws RecognitionException {
		FieldPartContext _localctx = new FieldPartContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_fieldPart);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246); ((FieldPartContext)_localctx).Id = match(Id);
			setState(249);
			_la = _input.LA(1);
			if (_la==19) {
				{
				setState(247); ((FieldPartContext)_localctx).e = match(19);
				setState(248); ((FieldPartContext)_localctx).expression = expression();
				}
			}


					Field field = new Field(new Source(((FieldPartContext)_localctx).Id), Type.INVALID, (((FieldPartContext)_localctx).Id!=null?((FieldPartContext)_localctx).Id.getText():null));
					field.initial = ((FieldPartContext)_localctx).e == null ? null : ((FieldPartContext)_localctx).expression.value;
					if (field.initial != null) field.type = field.initial.type;
					((FieldPartContext)_localctx).fld =  field;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarPartContext extends ParserRuleContext {
		public Variable var;
		public Token Id;
		public Token e;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public VarPartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varPart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterVarPart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitVarPart(this);
		}
	}

	public final VarPartContext varPart() throws RecognitionException {
		VarPartContext _localctx = new VarPartContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_varPart);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(253); ((VarPartContext)_localctx).Id = match(Id);
			setState(256);
			_la = _input.LA(1);
			if (_la==19) {
				{
				setState(254); ((VarPartContext)_localctx).e = match(19);
				setState(255); ((VarPartContext)_localctx).expression = expression();
				}
			}


					Variable variable = new Variable(new Source(((VarPartContext)_localctx).Id), Type.INVALID, (((VarPartContext)_localctx).Id!=null?((VarPartContext)_localctx).Id.getText():null));
					variable.initial = ((VarPartContext)_localctx).e == null ? null : ((VarPartContext)_localctx).expression.value;
					if (variable.initial != null) variable.type = variable.initial.type;
					((VarPartContext)_localctx).var =  variable;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public Token ifkeyword;
		public ConditionContext condition;
		public Token last;
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitIfStatement(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_ifStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(260); ((IfStatementContext)_localctx).ifkeyword = match(6);

					IfStatement ifStat = new IfStatement(new Source(((IfStatementContext)_localctx).ifkeyword));
					scope.enter(ifStat);
				
			setState(262); match(4);
			setState(263); ((IfStatementContext)_localctx).condition = condition();
			setState(264); ((IfStatementContext)_localctx).last = match(15);

					ifStat.setCondition(((IfStatementContext)_localctx).condition.value);
					Scope ifPart = new Scope(((IfStatementContext)_localctx).last, "ifpart-"+Node.genID());
					ifStat.setIfPart(ifPart);
					scope.enter(ifPart);
				
			setState(266); block();

					scope.leave(ifPart);
					scope.leave(ifStat);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public VariableValueContext variableValue;
		public Token eq;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableValueContext variableValue() {
			return getRuleContext(VariableValueContext.class,0);
		}
		public EolContext eol() {
			return getRuleContext(EolContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitAssignment(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(269); ((AssignmentContext)_localctx).variableValue = variableValue();
			setState(270); ((AssignmentContext)_localctx).eq = match(19);
			setState(271); ((AssignmentContext)_localctx).expression = expression();
			setState(272); eol();

					Assignment assign = new Assignment(new Source(((AssignmentContext)_localctx).eq));
					scope.add(assign);
					
					assign.left = ((AssignmentContext)_localctx).variableValue.value;
					assign.right = ((AssignmentContext)_localctx).expression.value;
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatementContext extends ParserRuleContext {
		public Token r;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public EolContext eol() {
			return getRuleContext(EolContext.class,0);
		}
		public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitReturnStatement(this);
		}
	}

	public final ReturnStatementContext returnStatement() throws RecognitionException {
		ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_returnStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(275); ((ReturnStatementContext)_localctx).r = match(18);
			setState(276); ((ReturnStatementContext)_localctx).expression = expression();
			setState(277); eol();

					Return ret = new Return(new Source(((ReturnStatementContext)_localctx).r));
					ret.value = ((ReturnStatementContext)_localctx).expression.value;
					scope.add(ret);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifiersContext extends ParserRuleContext {
		public Set<Modifier> mods;
		public ModifierContext modifier;
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public ModifiersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifiers; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterModifiers(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitModifiers(this);
		}
	}

	public final ModifiersContext modifiers() throws RecognitionException {
		ModifiersContext _localctx = new ModifiersContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_modifiers);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			((ModifiersContext)_localctx).mods =  new TreeSet<Modifier>();
			setState(286);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==13) {
				{
				{
				setState(281); ((ModifiersContext)_localctx).modifier = modifier();
				_localctx.mods.add(((ModifiersContext)_localctx).modifier.value);
				}
				}
				setState(288);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifierContext extends ParserRuleContext {
		public Modifier value;
		public ModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitModifier(this);
		}
	}

	public final ModifierContext modifier() throws RecognitionException {
		ModifierContext _localctx = new ModifierContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_modifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289); match(13);
			((ModifierContext)_localctx).value =  Modifier.STATIC;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public Value value;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitCondition(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(292); ((ConditionContext)_localctx).expression = expression();
			((ConditionContext)_localctx).value =  ((ConditionContext)_localctx).expression.value;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Value value;
		public PolynomialContext a;
		public Token op;
		public PolynomialContext b;
		public PolynomialContext polynomial;
		public PolynomialContext polynomial(int i) {
			return getRuleContext(PolynomialContext.class,i);
		}
		public List<PolynomialContext> polynomial() {
			return getRuleContexts(PolynomialContext.class);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_expression);
		try {
			setState(303);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(295); ((ExpressionContext)_localctx).a = polynomial(0);
				setState(296); ((ExpressionContext)_localctx).op = match(24);
				setState(297); ((ExpressionContext)_localctx).b = polynomial(0);
				((ExpressionContext)_localctx).value =  binaryOp(((ExpressionContext)_localctx).op, BinaryOp.Operation.EQUAL, ((ExpressionContext)_localctx).a.value, ((ExpressionContext)_localctx).b.value);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(300); ((ExpressionContext)_localctx).polynomial = polynomial(0);
				((ExpressionContext)_localctx).value =  ((ExpressionContext)_localctx).polynomial.value;
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PolynomialContext extends ParserRuleContext {
		public int _p;
		public Value value;
		public PolynomialContext a;
		public TermContext term;
		public Token op;
		public TermContext b;
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public PolynomialContext polynomial() {
			return getRuleContext(PolynomialContext.class,0);
		}
		public PolynomialContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public PolynomialContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_polynomial; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterPolynomial(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitPolynomial(this);
		}
	}

	public final PolynomialContext polynomial(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PolynomialContext _localctx = new PolynomialContext(_ctx, _parentState, _p);
		PolynomialContext _prevctx = _localctx;
		int _startState = 44;
		enterRecursionRule(_localctx, RULE_polynomial);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(306); ((PolynomialContext)_localctx).term = term(0);
			((PolynomialContext)_localctx).value =  ((PolynomialContext)_localctx).term.value;
			}
			_ctx.stop = _input.LT(-1);
			setState(321);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(319);
					switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
					case 1:
						{
						_localctx = new PolynomialContext(_parentctx, _parentState, _p);
						_localctx.a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_polynomial);
						setState(309);
						if (!(3 >= _localctx._p)) throw new FailedPredicateException(this, "3 >= $_p");
						setState(310); ((PolynomialContext)_localctx).op = match(17);
						setState(311); ((PolynomialContext)_localctx).b = ((PolynomialContext)_localctx).term = term(0);
						((PolynomialContext)_localctx).value =  binaryOp(((PolynomialContext)_localctx).op, BinaryOp.Operation.ADD, ((PolynomialContext)_localctx).a.value, ((PolynomialContext)_localctx).b.value);
						}
						break;

					case 2:
						{
						_localctx = new PolynomialContext(_parentctx, _parentState, _p);
						_localctx.a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_polynomial);
						setState(314);
						if (!(2 >= _localctx._p)) throw new FailedPredicateException(this, "2 >= $_p");
						setState(315); ((PolynomialContext)_localctx).op = match(3);
						setState(316); ((PolynomialContext)_localctx).b = ((PolynomialContext)_localctx).term = term(0);
						((PolynomialContext)_localctx).value =  binaryOp(((PolynomialContext)_localctx).op, BinaryOp.Operation.SUB, ((PolynomialContext)_localctx).a.value, ((PolynomialContext)_localctx).b.value);
						}
						break;
					}
					} 
				}
				setState(323);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public int _p;
		public Value value;
		public TermContext a;
		public AtomContext atom;
		public Token op;
		public AtomContext b;
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TermContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public TermContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitTerm(this);
		}
	}

	public final TermContext term(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TermContext _localctx = new TermContext(_ctx, _parentState, _p);
		TermContext _prevctx = _localctx;
		int _startState = 46;
		enterRecursionRule(_localctx, RULE_term);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(325); ((TermContext)_localctx).atom = atom();
			((TermContext)_localctx).value =  ((TermContext)_localctx).atom.value;
			}
			_ctx.stop = _input.LT(-1);
			setState(340);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(338);
					switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
					case 1:
						{
						_localctx = new TermContext(_parentctx, _parentState, _p);
						_localctx.a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(328);
						if (!(3 >= _localctx._p)) throw new FailedPredicateException(this, "3 >= $_p");
						setState(329); ((TermContext)_localctx).op = match(23);
						setState(330); ((TermContext)_localctx).b = ((TermContext)_localctx).atom = atom();
						((TermContext)_localctx).value =  binaryOp(((TermContext)_localctx).op, BinaryOp.Operation.DIV, ((TermContext)_localctx).a.value, ((TermContext)_localctx).b.value);
						}
						break;

					case 2:
						{
						_localctx = new TermContext(_parentctx, _parentState, _p);
						_localctx.a = _prevctx;
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(333);
						if (!(2 >= _localctx._p)) throw new FailedPredicateException(this, "2 >= $_p");
						setState(334); ((TermContext)_localctx).op = match(2);
						setState(335); ((TermContext)_localctx).b = ((TermContext)_localctx).atom = atom();
						((TermContext)_localctx).value =  binaryOp(((TermContext)_localctx).op, BinaryOp.Operation.MUL, ((TermContext)_localctx).a.value, ((TermContext)_localctx).b.value);
						}
						break;
					}
					} 
				}
				setState(342);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public Value value;
		public ExpressionContext expression;
		public CallContext call;
		public VariableValueContext variableValue;
		public Token Integer;
		public Token Float;
		public Token String;
		public TerminalNode Float() { return getToken(RooibosParser.Float, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public VariableValueContext variableValue() {
			return getRuleContext(VariableValueContext.class,0);
		}
		public TerminalNode String() { return getToken(RooibosParser.String, 0); }
		public TerminalNode Integer() { return getToken(RooibosParser.Integer, 0); }
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_atom);
		try {
			setState(360);
			switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(343); match(4);
				setState(344); ((AtomContext)_localctx).expression = expression();
				setState(345); match(15);
				((AtomContext)_localctx).value =  ((AtomContext)_localctx).expression.value;
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(348); ((AtomContext)_localctx).call = call();
				((AtomContext)_localctx).value =  ((AtomContext)_localctx).call.value;
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(351); ((AtomContext)_localctx).variableValue = variableValue();
				((AtomContext)_localctx).value =  ((AtomContext)_localctx).variableValue.value;
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(354); ((AtomContext)_localctx).Integer = match(Integer);
				((AtomContext)_localctx).value =  new Literal(new Source(((AtomContext)_localctx).Integer), scope.findType("int"), Long.parseLong((((AtomContext)_localctx).Integer!=null?((AtomContext)_localctx).Integer.getText():null))); _localctx.value.setConst();
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(356); ((AtomContext)_localctx).Float = match(Float);
				((AtomContext)_localctx).value =  new Literal(new Source(((AtomContext)_localctx).Float), scope.findType("float"), Double.parseDouble((((AtomContext)_localctx).Float!=null?((AtomContext)_localctx).Float.getText():null))); _localctx.value.setConst();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(358); ((AtomContext)_localctx).String = match(String);
				((AtomContext)_localctx).value =  new Literal(new Source(((AtomContext)_localctx).String), scope.findType("string"), (((AtomContext)_localctx).String!=null?((AtomContext)_localctx).String.getText():null)); _localctx.value.setConst();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallStatementContext extends ParserRuleContext {
		public CallContext call;
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public EolContext eol() {
			return getRuleContext(EolContext.class,0);
		}
		public CallStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterCallStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitCallStatement(this);
		}
	}

	public final CallStatementContext callStatement() throws RecognitionException {
		CallStatementContext _localctx = new CallStatementContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_callStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(362); ((CallStatementContext)_localctx).call = call();
			setState(363); eol();

					Value value = ((CallStatementContext)_localctx).call.value;
					if (value == null) value = new Value(Source.UNKNOWN, Type.INVALID);
					Statement stat = new Statement(value.source, value.name, value);
					scope.add(stat);
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public Value value;
		public ScopingContext scoping;
		public Token Id;
		public List<ArgumentContext> argument() {
			return getRuleContexts(ArgumentContext.class);
		}
		public ScopingContext scoping() {
			return getRuleContext(ScopingContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public ArgumentContext argument(int i) {
			return getRuleContext(ArgumentContext.class,i);
		}
		public CallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitCall(this);
		}
	}

	public final CallContext call() throws RecognitionException {
		CallContext _localctx = new CallContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(366); ((CallContext)_localctx).scoping = scoping();
			setState(367); ((CallContext)_localctx).Id = match(Id);

					Call callValue = null;
					Node node = ((CallContext)_localctx).scoping.currentScope.find((((CallContext)_localctx).Id!=null?((CallContext)_localctx).Id.getText():null));
							
					if (node instanceof Method) {
						callValue = new MethodCall(new Source(((CallContext)_localctx).Id), (Method)node);
					}
					else if (node instanceof Function) {
						callValue = new FunctionCall(new Source(((CallContext)_localctx).Id), (Function)node);
					}
					else if (node instanceof Class) {
						callValue = new ConstructorCall(new Source(((CallContext)_localctx).Id), (Class)node);
					}
					else {
						String errMsg = "Could not find '" + (((CallContext)_localctx).Id!=null?((CallContext)_localctx).Id.getText():null) + "'";
						if (((CallContext)_localctx).scoping.currentScope != scope.current) errMsg += " in '" + ((CallContext)_localctx).scoping.currentScope.name + "'";
						reporter.error(new Source(((CallContext)_localctx).Id), errMsg);
						((CallContext)_localctx).value =  new Value(new Source(((CallContext)_localctx).Id), Type.INVALID);
					}
				
			setState(369); match(4);
			setState(378);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 4) | (1L << Id) | (1L << Integer) | (1L << Float) | (1L << String))) != 0)) {
				{
				setState(370); argument(callValue);
				setState(375);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==1) {
					{
					{
					setState(371); match(1);
					setState(372); argument(callValue);
					}
					}
					setState(377);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(380); match(15);

					((CallContext)_localctx).value =  callValue;
					
					if (_localctx.value == null) ((CallContext)_localctx).value =  new Value(new Source(((CallContext)_localctx).Id), Type.INVALID);
					
					if (((CallContext)_localctx).scoping.nodes.peek() == scope.current) ((CallContext)_localctx).scoping.nodes.pop();
					while (!((CallContext)_localctx).scoping.nodes.isEmpty()) {
						node = ((CallContext)_localctx).scoping.nodes.pop();
						if (node == null) continue;
						((CallContext)_localctx).value =  new ScopeAccess(_localctx.value.source, node, _localctx.value);
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentContext extends ParserRuleContext {
		public Call callValue;
		public ExpressionContext expression;
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArgumentContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ArgumentContext(ParserRuleContext parent, int invokingState, Call callValue) {
			super(parent, invokingState);
			this.callValue = callValue;
		}
		@Override public int getRuleIndex() { return RULE_argument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitArgument(this);
		}
	}

	public final ArgumentContext argument(Call callValue) throws RecognitionException {
		ArgumentContext _localctx = new ArgumentContext(_ctx, getState(), callValue);
		enterRule(_localctx, 54, RULE_argument);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383); ((ArgumentContext)_localctx).expression = expression();
			if (callValue != null) callValue.arguments.add(((ArgumentContext)_localctx).expression.value);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Variable var;
		public Token Id;
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(386); ((VariableContext)_localctx).Id = match(Id);

					((VariableContext)_localctx).var =  scope.findVariable((((VariableContext)_localctx).Id!=null?((VariableContext)_localctx).Id.getText():null));
					if (_localctx.var == null) {
						reporter.error(new Source(((VariableContext)_localctx).Id), "Could not find '" + (((VariableContext)_localctx).Id!=null?((VariableContext)_localctx).Id.getText():null) + "'");
						((VariableContext)_localctx).var =  Variable.INVALID;
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableValueContext extends ParserRuleContext {
		public VariableValue value;
		public ScopingContext scoping;
		public Token Id;
		public ScopingContext scoping() {
			return getRuleContext(ScopingContext.class,0);
		}
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public VariableValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterVariableValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitVariableValue(this);
		}
	}

	public final VariableValueContext variableValue() throws RecognitionException {
		VariableValueContext _localctx = new VariableValueContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_variableValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(389); ((VariableValueContext)_localctx).scoping = scoping();
			setState(390); ((VariableValueContext)_localctx).Id = match(Id);
				
					Variable var = ((VariableValueContext)_localctx).scoping.currentScope.findVariable((((VariableValueContext)_localctx).Id!=null?((VariableValueContext)_localctx).Id.getText():null));
					if (var == null) {
						reporter.error(new Source(((VariableValueContext)_localctx).Id), "Unknown variable '" + (((VariableValueContext)_localctx).Id!=null?((VariableValueContext)_localctx).Id.getText():null) + "'");
						((VariableValueContext)_localctx).value =  new VariableValue(new Source(((VariableValueContext)_localctx).Id), Variable.INVALID);
					}
					else {
						((VariableValueContext)_localctx).value =  new VariableValue(new Source(((VariableValueContext)_localctx).Id), var);

						if (((VariableValueContext)_localctx).scoping.nodes.peek() == scope.current) ((VariableValueContext)_localctx).scoping.nodes.pop();
						
						while (!((VariableValueContext)_localctx).scoping.nodes.isEmpty()) {
							Node node = ((VariableValueContext)_localctx).scoping.nodes.pop();
							if (node == null) continue;
							((VariableValueContext)_localctx).value =  new VariableScopeAccess(_localctx.value.source, node, _localctx.value);
						}
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type t;
		public TypeAtomContext typeAtom;
		public TypeContext b;
		public TypeContext type;
		public TypeAtomContext typeAtom() {
			return getRuleContext(TypeAtomContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_type);
		try {
			setState(408);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{

						UnionType ut = new UnionType("[union type]");
					
				setState(394); ((TypeContext)_localctx).typeAtom = typeAtom();
				setState(395); match(26);
				setState(396); ((TypeContext)_localctx).b = type();
				ut.addType(((TypeContext)_localctx).typeAtom.t); ut.addType(((TypeContext)_localctx).b.t);

						((TypeContext)_localctx).t =  ut;
					
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(400); match(4);
				setState(401); ((TypeContext)_localctx).type = type();
				setState(402); match(15);
				((TypeContext)_localctx).t =  _localctx.t;
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(405); ((TypeContext)_localctx).typeAtom = typeAtom();
				((TypeContext)_localctx).t =  ((TypeContext)_localctx).typeAtom.t;
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeAtomContext extends ParserRuleContext {
		public Type t;
		public Token Id;
		public TerminalNode Id() { return getToken(RooibosParser.Id, 0); }
		public TypeAtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeAtom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterTypeAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitTypeAtom(this);
		}
	}

	public final TypeAtomContext typeAtom() throws RecognitionException {
		TypeAtomContext _localctx = new TypeAtomContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_typeAtom);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(410); ((TypeAtomContext)_localctx).Id = match(Id);

					((TypeAtomContext)_localctx).t =  scope.findType((((TypeAtomContext)_localctx).Id!=null?((TypeAtomContext)_localctx).Id.getText():null));
					if (_localctx.t == null) {
						reporter.error(new Source(((TypeAtomContext)_localctx).Id), "Unknown type '" + (((TypeAtomContext)_localctx).Id!=null?((TypeAtomContext)_localctx).Id.getText():null) + "'");
						((TypeAtomContext)_localctx).t =  Type.TOP;
					}
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScopingContext extends ParserRuleContext {
		public LinkedList<Node> nodes;
		public Scope currentScope;
		public Token Id;
		public TerminalNode Id(int i) {
			return getToken(RooibosParser.Id, i);
		}
		public List<TerminalNode> Id() { return getTokens(RooibosParser.Id); }
		public ScopingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scoping; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterScoping(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitScoping(this);
		}
	}

	public final ScopingContext scoping() throws RecognitionException {
		ScopingContext _localctx = new ScopingContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_scoping);

					((ScopingContext)_localctx).nodes =  new LinkedList<Node>();
					Node curNode = scope.current;
					((ScopingContext)_localctx).currentScope =  scope.current;
				
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(418);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(413); ((ScopingContext)_localctx).Id = match(Id);

								Scope prevScope = (Scope)curNode;
								
								if ((((ScopingContext)_localctx).Id!=null?((ScopingContext)_localctx).Id.getText():null).equals("this")) {
									Type type = scope.getEnclosingType();
									curNode = type;
									_localctx.nodes.add(type);
								}
								else {
									curNode = prevScope.find((((ScopingContext)_localctx).Id!=null?((ScopingContext)_localctx).Id.getText():null));
									if (curNode == null) 
										reporter.error(new Source(((ScopingContext)_localctx).Id), "Could not find '" + (((ScopingContext)_localctx).Id!=null?((ScopingContext)_localctx).Id.getText():null) + "' in scope of '" + prevScope + "'");
									else
										_localctx.nodes.add(curNode);
								}
							
					setState(415); match(16);
					}
					} 
				}
				setState(420);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,34,_ctx);
			}

					if (_localctx.nodes.isEmpty()) _localctx.nodes.add(scope.current);
						
					curNode = _localctx.nodes.getLast();
					if (curNode instanceof Scope) {
						((ScopingContext)_localctx).currentScope =  (Scope)curNode;
					}
					else if (curNode instanceof Variable) {
						((ScopingContext)_localctx).currentScope =  ((Variable)curNode).type;
					}
					else
						reporter.error(new Source(((ScopingContext)_localctx).Id), "'" + curNode + "' does not provide scope");
				
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EolContext extends ParserRuleContext {
		public EolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_eol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).enterEol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RooibosListener ) ((RooibosListener)listener).exitEol(this);
		}
	}

	public final EolContext eol() throws RecognitionException {
		EolContext _localctx = new EolContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_eol);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(424);
			_la = _input.LA(1);
			if (_la==20) {
				{
				setState(423); match(20);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 22: return polynomial_sempred((PolynomialContext)_localctx, predIndex);

		case 23: return term_sempred((TermContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean term_sempred(TermContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return 3 >= _localctx._p;

		case 3: return 2 >= _localctx._p;
		}
		return true;
	}
	private boolean polynomial_sempred(PolynomialContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return 3 >= _localctx._p;

		case 1: return 2 >= _localctx._p;
		}
		return true;
	}

	public static final String _serializedATN =
		"\2\3!\u01ad\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4"+
		"\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20"+
		"\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27"+
		"\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36"+
		"\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\7"+
		"\4O\n\4\f\4\16\4R\13\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5]\n\5\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6f\n\6\3\6\3\6\3\6\7\6k\n\6\f\6\16\6n\13"+
		"\6\3\6\5\6q\n\6\3\6\7\6t\n\6\f\6\16\6w\13\6\3\6\3\6\3\6\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\b\3\b\5\b\u0084\n\b\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u008c\n\t"+
		"\f\t\16\t\u008f\13\t\5\t\u0091\n\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\7\n\u009f\n\n\f\n\16\n\u00a2\13\n\5\n\u00a4\n\n\3\n\3\n\3"+
		"\n\5\n\u00a9\n\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00b5"+
		"\n\13\f\13\16\13\u00b8\13\13\5\13\u00ba\n\13\3\13\3\13\3\13\5\13\u00bf"+
		"\n\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\5\r\u00cd\n\r"+
		"\3\r\3\r\3\r\7\r\u00d2\n\r\f\r\16\r\u00d5\13\r\3\r\3\r\3\r\3\r\5\r\u00db"+
		"\n\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\5\16\u00e4\n\16\3\16\3\16\3\16\7"+
		"\16\u00e9\n\16\f\16\16\16\u00ec\13\16\3\16\3\16\3\16\3\16\5\16\u00f2\n"+
		"\16\3\16\5\16\u00f5\n\16\3\16\3\16\3\17\3\17\3\17\5\17\u00fc\n\17\3\17"+
		"\3\17\3\20\3\20\3\20\5\20\u0103\n\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23"+
		"\3\23\3\24\3\24\3\24\3\24\7\24\u011f\n\24\f\24\16\24\u0122\13\24\3\25"+
		"\3\25\3\25\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\5\27"+
		"\u0132\n\27\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\7\30\u0142\n\30\f\30\16\30\u0145\13\30\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0155\n\31\f\31"+
		"\16\31\u0158\13\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3"+
		"\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u016b\n\32\3\33\3\33\3\33\3\33"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0178\n\34\f\34\16\34\u017b\13"+
		"\34\5\34\u017d\n\34\3\34\3\34\3\34\3\35\3\35\3\35\3\36\3\36\3\36\3\37"+
		"\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \5 \u019b"+
		"\n \3!\3!\3!\3\"\3\"\3\"\7\"\u01a3\n\"\f\"\16\"\u01a6\13\"\3\"\3\"\3#"+
		"\5#\u01ab\n#\3#\2$\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60"+
		"\62\64\668:<>@BD\2\2\u01ba\2F\3\2\2\2\4I\3\2\2\2\6P\3\2\2\2\b\\\3\2\2"+
		"\2\n^\3\2\2\2\f{\3\2\2\2\16\u0083\3\2\2\2\20\u0085\3\2\2\2\22\u0096\3"+
		"\2\2\2\24\u00ad\3\2\2\2\26\u00c3\3\2\2\2\30\u00c8\3\2\2\2\32\u00df\3\2"+
		"\2\2\34\u00f8\3\2\2\2\36\u00ff\3\2\2\2 \u0106\3\2\2\2\"\u010f\3\2\2\2"+
		"$\u0115\3\2\2\2&\u011a\3\2\2\2(\u0123\3\2\2\2*\u0126\3\2\2\2,\u0131\3"+
		"\2\2\2.\u0133\3\2\2\2\60\u0146\3\2\2\2\62\u016a\3\2\2\2\64\u016c\3\2\2"+
		"\2\66\u0170\3\2\2\28\u0181\3\2\2\2:\u0184\3\2\2\2<\u0187\3\2\2\2>\u019a"+
		"\3\2\2\2@\u019c\3\2\2\2B\u01a4\3\2\2\2D\u01aa\3\2\2\2FG\5\6\4\2GH\7\1"+
		"\2\2H\3\3\2\2\2IJ\7\n\2\2JK\5\6\4\2KL\7\16\2\2L\5\3\2\2\2MO\5\b\5\2NM"+
		"\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3\2\2\2Q\7\3\2\2\2RP\3\2\2\2S]\7\35\2\2"+
		"T]\5\24\13\2U]\5\32\16\2V]\5\"\22\2W]\5$\23\2X]\5\64\33\2Y]\5 \21\2Z]"+
		"\5\n\6\2[]\5\f\7\2\\S\3\2\2\2\\T\3\2\2\2\\U\3\2\2\2\\V\3\2\2\2\\W\3\2"+
		"\2\2\\X\3\2\2\2\\Y\3\2\2\2\\Z\3\2\2\2\\[\3\2\2\2]\t\3\2\2\2^_\7\33\2\2"+
		"_`\7\35\2\2`e\b\6\1\2ab\7\f\2\2bc\5@!\2cd\b\6\1\2df\3\2\2\2ea\3\2\2\2"+
		"ef\3\2\2\2fg\3\2\2\2gh\b\6\1\2hl\7\n\2\2ik\5\16\b\2ji\3\2\2\2kn\3\2\2"+
		"\2lj\3\2\2\2lm\3\2\2\2mp\3\2\2\2nl\3\2\2\2oq\5\20\t\2po\3\2\2\2pq\3\2"+
		"\2\2qu\3\2\2\2rt\5\16\b\2sr\3\2\2\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2vx\3"+
		"\2\2\2wu\3\2\2\2xy\7\16\2\2yz\b\6\1\2z\13\3\2\2\2{|\7\27\2\2|}\7\35\2"+
		"\2}~\7\25\2\2~\177\5> \2\177\u0080\b\7\1\2\u0080\r\3\2\2\2\u0081\u0084"+
		"\5\22\n\2\u0082\u0084\5\30\r\2\u0083\u0081\3\2\2\2\u0083\u0082\3\2\2\2"+
		"\u0084\17\3\2\2\2\u0085\u0086\7\r\2\2\u0086\u0087\b\t\1\2\u0087\u0090"+
		"\7\6\2\2\u0088\u008d\5\26\f\2\u0089\u008a\7\3\2\2\u008a\u008c\5\26\f\2"+
		"\u008b\u0089\3\2\2\2\u008c\u008f\3\2\2\2\u008d\u008b\3\2\2\2\u008d\u008e"+
		"\3\2\2\2\u008e\u0091\3\2\2\2\u008f\u008d\3\2\2\2\u0090\u0088\3\2\2\2\u0090"+
		"\u0091\3\2\2\2\u0091\u0092\3\2\2\2\u0092\u0093\7\21\2\2\u0093\u0094\5"+
		"\4\3\2\u0094\u0095\b\t\1\2\u0095\21\3\2\2\2\u0096\u0097\5&\24\2\u0097"+
		"\u0098\7\13\2\2\u0098\u0099\7\35\2\2\u0099\u009a\b\n\1\2\u009a\u00a3\7"+
		"\6\2\2\u009b\u00a0\5\26\f\2\u009c\u009d\7\3\2\2\u009d\u009f\5\26\f\2\u009e"+
		"\u009c\3\2\2\2\u009f\u00a2\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u00a1\3\2"+
		"\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a3\u009b\3\2\2\2\u00a3"+
		"\u00a4\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a8\7\21\2\2\u00a6\u00a7\7"+
		"\7\2\2\u00a7\u00a9\5> \2\u00a8\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9"+
		"\u00aa\3\2\2\2\u00aa\u00ab\5\4\3\2\u00ab\u00ac\b\n\1\2\u00ac\23\3\2\2"+
		"\2\u00ad\u00ae\7\20\2\2\u00ae\u00af\7\35\2\2\u00af\u00b0\b\13\1\2\u00b0"+
		"\u00b9\7\6\2\2\u00b1\u00b6\5\26\f\2\u00b2\u00b3\7\3\2\2\u00b3\u00b5\5"+
		"\26\f\2\u00b4\u00b2\3\2\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b4\3\2\2\2\u00b6"+
		"\u00b7\3\2\2\2\u00b7\u00ba\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b9\u00b1\3\2"+
		"\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00be\7\21\2\2\u00bc"+
		"\u00bd\7\7\2\2\u00bd\u00bf\5> \2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2"+
		"\2\u00bf\u00c0\3\2\2\2\u00c0\u00c1\5\4\3\2\u00c1\u00c2\b\13\1\2\u00c2"+
		"\25\3\2\2\2\u00c3\u00c4\7\35\2\2\u00c4\u00c5\7\7\2\2\u00c5\u00c6\5> \2"+
		"\u00c6\u00c7\b\f\1\2\u00c7\27\3\2\2\2\u00c8\u00cc\b\r\1\2\u00c9\u00cd"+
		"\7\t\2\2\u00ca\u00cb\7\30\2\2\u00cb\u00cd\b\r\1\2\u00cc\u00c9\3\2\2\2"+
		"\u00cc\u00ca\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00d3\5\34\17\2\u00cf\u00d0"+
		"\7\3\2\2\u00d0\u00d2\5\34\17\2\u00d1\u00cf\3\2\2\2\u00d2\u00d5\3\2\2\2"+
		"\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00da\3\2\2\2\u00d5\u00d3"+
		"\3\2\2\2\u00d6\u00d7\7\7\2\2\u00d7\u00d8\5> \2\u00d8\u00d9\b\r\1\2\u00d9"+
		"\u00db\3\2\2\2\u00da\u00d6\3\2\2\2\u00da\u00db\3\2\2\2\u00db\u00dc\3\2"+
		"\2\2\u00dc\u00dd\5D#\2\u00dd\u00de\b\r\1\2\u00de\31\3\2\2\2\u00df\u00e3"+
		"\b\16\1\2\u00e0\u00e4\7\t\2\2\u00e1\u00e2\7\30\2\2\u00e2\u00e4\b\16\1"+
		"\2\u00e3\u00e0\3\2\2\2\u00e3\u00e1\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00ea"+
		"\5\36\20\2\u00e6\u00e7\7\3\2\2\u00e7\u00e9\5\36\20\2\u00e8\u00e6\3\2\2"+
		"\2\u00e9\u00ec\3\2\2\2\u00ea\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00f1"+
		"\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ed\u00ee\7\7\2\2\u00ee\u00ef\5> \2\u00ef"+
		"\u00f0\b\16\1\2\u00f0\u00f2\3\2\2\2\u00f1\u00ed\3\2\2\2\u00f1\u00f2\3"+
		"\2\2\2\u00f2\u00f4\3\2\2\2\u00f3\u00f5\5D#\2\u00f4\u00f3\3\2\2\2\u00f4"+
		"\u00f5\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f7\b\16\1\2\u00f7\33\3\2\2"+
		"\2\u00f8\u00fb\7\35\2\2\u00f9\u00fa\7\25\2\2\u00fa\u00fc\5,\27\2\u00fb"+
		"\u00f9\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00fe\b\17"+
		"\1\2\u00fe\35\3\2\2\2\u00ff\u0102\7\35\2\2\u0100\u0101\7\25\2\2\u0101"+
		"\u0103\5,\27\2\u0102\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0104\3\2"+
		"\2\2\u0104\u0105\b\20\1\2\u0105\37\3\2\2\2\u0106\u0107\7\b\2\2\u0107\u0108"+
		"\b\21\1\2\u0108\u0109\7\6\2\2\u0109\u010a\5*\26\2\u010a\u010b\7\21\2\2"+
		"\u010b\u010c\b\21\1\2\u010c\u010d\5\4\3\2\u010d\u010e\b\21\1\2\u010e!"+
		"\3\2\2\2\u010f\u0110\5<\37\2\u0110\u0111\7\25\2\2\u0111\u0112\5,\27\2"+
		"\u0112\u0113\5D#\2\u0113\u0114\b\22\1\2\u0114#\3\2\2\2\u0115\u0116\7\24"+
		"\2\2\u0116\u0117\5,\27\2\u0117\u0118\5D#\2\u0118\u0119\b\23\1\2\u0119"+
		"%\3\2\2\2\u011a\u0120\b\24\1\2\u011b\u011c\5(\25\2\u011c\u011d\b\24\1"+
		"\2\u011d\u011f\3\2\2\2\u011e\u011b\3\2\2\2\u011f\u0122\3\2\2\2\u0120\u011e"+
		"\3\2\2\2\u0120\u0121\3\2\2\2\u0121\'\3\2\2\2\u0122\u0120\3\2\2\2\u0123"+
		"\u0124\7\17\2\2\u0124\u0125\b\25\1\2\u0125)\3\2\2\2\u0126\u0127\5,\27"+
		"\2\u0127\u0128\b\26\1\2\u0128+\3\2\2\2\u0129\u012a\5.\30\2\u012a\u012b"+
		"\7\32\2\2\u012b\u012c\5.\30\2\u012c\u012d\b\27\1\2\u012d\u0132\3\2\2\2"+
		"\u012e\u012f\5.\30\2\u012f\u0130\b\27\1\2\u0130\u0132\3\2\2\2\u0131\u0129"+
		"\3\2\2\2\u0131\u012e\3\2\2\2\u0132-\3\2\2\2\u0133\u0134\b\30\1\2\u0134"+
		"\u0135\5\60\31\2\u0135\u0136\b\30\1\2\u0136\u0143\3\2\2\2\u0137\u0138"+
		"\6\30\2\3\u0138\u0139\7\23\2\2\u0139\u013a\5\60\31\2\u013a\u013b\b\30"+
		"\1\2\u013b\u0142\3\2\2\2\u013c\u013d\6\30\3\3\u013d\u013e\7\5\2\2\u013e"+
		"\u013f\5\60\31\2\u013f\u0140\b\30\1\2\u0140\u0142\3\2\2\2\u0141\u0137"+
		"\3\2\2\2\u0141\u013c\3\2\2\2\u0142\u0145\3\2\2\2\u0143\u0141\3\2\2\2\u0143"+
		"\u0144\3\2\2\2\u0144/\3\2\2\2\u0145\u0143\3\2\2\2\u0146\u0147\b\31\1\2"+
		"\u0147\u0148\5\62\32\2\u0148\u0149\b\31\1\2\u0149\u0156\3\2\2\2\u014a"+
		"\u014b\6\31\4\3\u014b\u014c\7\31\2\2\u014c\u014d\5\62\32\2\u014d\u014e"+
		"\b\31\1\2\u014e\u0155\3\2\2\2\u014f\u0150\6\31\5\3\u0150\u0151\7\4\2\2"+
		"\u0151\u0152\5\62\32\2\u0152\u0153\b\31\1\2\u0153\u0155\3\2\2\2\u0154"+
		"\u014a\3\2\2\2\u0154\u014f\3\2\2\2\u0155\u0158\3\2\2\2\u0156\u0154\3\2"+
		"\2\2\u0156\u0157\3\2\2\2\u0157\61\3\2\2\2\u0158\u0156\3\2\2\2\u0159\u015a"+
		"\7\6\2\2\u015a\u015b\5,\27\2\u015b\u015c\7\21\2\2\u015c\u015d\b\32\1\2"+
		"\u015d\u016b\3\2\2\2\u015e\u015f\5\66\34\2\u015f\u0160\b\32\1\2\u0160"+
		"\u016b\3\2\2\2\u0161\u0162\5<\37\2\u0162\u0163\b\32\1\2\u0163\u016b\3"+
		"\2\2\2\u0164\u0165\7\36\2\2\u0165\u016b\b\32\1\2\u0166\u0167\7\37\2\2"+
		"\u0167\u016b\b\32\1\2\u0168\u0169\7 \2\2\u0169\u016b\b\32\1\2\u016a\u0159"+
		"\3\2\2\2\u016a\u015e\3\2\2\2\u016a\u0161\3\2\2\2\u016a\u0164\3\2\2\2\u016a"+
		"\u0166\3\2\2\2\u016a\u0168\3\2\2\2\u016b\63\3\2\2\2\u016c\u016d\5\66\34"+
		"\2\u016d\u016e\5D#\2\u016e\u016f\b\33\1\2\u016f\65\3\2\2\2\u0170\u0171"+
		"\5B\"\2\u0171\u0172\7\35\2\2\u0172\u0173\b\34\1\2\u0173\u017c\7\6\2\2"+
		"\u0174\u0179\58\35\2\u0175\u0176\7\3\2\2\u0176\u0178\58\35\2\u0177\u0175"+
		"\3\2\2\2\u0178\u017b\3\2\2\2\u0179\u0177\3\2\2\2\u0179\u017a\3\2\2\2\u017a"+
		"\u017d\3\2\2\2\u017b\u0179\3\2\2\2\u017c\u0174\3\2\2\2\u017c\u017d\3\2"+
		"\2\2\u017d\u017e\3\2\2\2\u017e\u017f\7\21\2\2\u017f\u0180\b\34\1\2\u0180"+
		"\67\3\2\2\2\u0181\u0182\5,\27\2\u0182\u0183\b\35\1\2\u01839\3\2\2\2\u0184"+
		"\u0185\7\35\2\2\u0185\u0186\b\36\1\2\u0186;\3\2\2\2\u0187\u0188\5B\"\2"+
		"\u0188\u0189\7\35\2\2\u0189\u018a\b\37\1\2\u018a=\3\2\2\2\u018b\u018c"+
		"\b \1\2\u018c\u018d\5@!\2\u018d\u018e\7\34\2\2\u018e\u018f\5> \2\u018f"+
		"\u0190\b \1\2\u0190\u0191\b \1\2\u0191\u019b\3\2\2\2\u0192\u0193\7\6\2"+
		"\2\u0193\u0194\5> \2\u0194\u0195\7\21\2\2\u0195\u0196\b \1\2\u0196\u019b"+
		"\3\2\2\2\u0197\u0198\5@!\2\u0198\u0199\b \1\2\u0199\u019b\3\2\2\2\u019a"+
		"\u018b\3\2\2\2\u019a\u0192\3\2\2\2\u019a\u0197\3\2\2\2\u019b?\3\2\2\2"+
		"\u019c\u019d\7\35\2\2\u019d\u019e\b!\1\2\u019eA\3\2\2\2\u019f\u01a0\7"+
		"\35\2\2\u01a0\u01a1\b\"\1\2\u01a1\u01a3\7\22\2\2\u01a2\u019f\3\2\2\2\u01a3"+
		"\u01a6\3\2\2\2\u01a4\u01a2\3\2\2\2\u01a4\u01a5\3\2\2\2\u01a5\u01a7\3\2"+
		"\2\2\u01a6\u01a4\3\2\2\2\u01a7\u01a8\b\"\1\2\u01a8C\3\2\2\2\u01a9\u01ab"+
		"\7\26\2\2\u01aa\u01a9\3\2\2\2\u01aa\u01ab\3\2\2\2\u01abE\3\2\2\2&P\\e"+
		"lpu\u0083\u008d\u0090\u00a0\u00a3\u00a8\u00b6\u00b9\u00be\u00cc\u00d3"+
		"\u00da\u00e3\u00ea\u00f1\u00f4\u00fb\u0102\u0120\u0131\u0141\u0143\u0154"+
		"\u0156\u016a\u0179\u017c\u019a\u01a4\u01aa";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}