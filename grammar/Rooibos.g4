grammar Rooibos;

@parser::header {
	import java.util.LinkedList;
	import java.util.Set;
	import java.util.TreeSet;

	import whitesquare.rooibos.compiler.nodes.Node;
	import whitesquare.rooibos.compiler.nodes.Scope;
	import whitesquare.rooibos.compiler.nodes.Constructor;
	import whitesquare.rooibos.compiler.nodes.Method;
	import whitesquare.rooibos.compiler.nodes.Function;
	import whitesquare.rooibos.compiler.nodes.Call;
	import whitesquare.rooibos.compiler.nodes.ConstructorCall;
	import whitesquare.rooibos.compiler.nodes.MethodCall;
	import whitesquare.rooibos.compiler.nodes.FunctionCall;
	import whitesquare.rooibos.compiler.nodes.Assignment;
	import whitesquare.rooibos.compiler.nodes.Statement;
	import whitesquare.rooibos.compiler.nodes.Return;
	import whitesquare.rooibos.compiler.nodes.Parameter;
	import whitesquare.rooibos.compiler.nodes.Field;
	import whitesquare.rooibos.compiler.nodes.ScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Variable;
	import whitesquare.rooibos.compiler.nodes.VariableValue;
	import whitesquare.rooibos.compiler.nodes.VariableScopeAccess;
	import whitesquare.rooibos.compiler.nodes.Value;
	import whitesquare.rooibos.compiler.nodes.Literal;
	import whitesquare.rooibos.compiler.nodes.BinaryOp;
	import whitesquare.rooibos.compiler.nodes.Source;
	import whitesquare.rooibos.compiler.nodes.IfStatement;
	import whitesquare.rooibos.compiler.nodes.Class;
	import whitesquare.rooibos.compiler.nodes.TypeAliasStatement;

	import whitesquare.rooibos.compiler.types.Modifier;
	import whitesquare.rooibos.compiler.types.Type;
	import whitesquare.rooibos.compiler.types.TypeAlias;
	import whitesquare.rooibos.compiler.types.UnionType;
}

@parser::members {
	public ErrorReporter reporter;
	public ScopeManager scope;
	
	private Value binaryOp(Token token, BinaryOp.Operation op, Value left, Value right) {
		if (left == null || right == null) {
			System.out.println(op.name());
			return new Value(new Source(token), Type.INVALID);
		}
		return new BinaryOp(new Source(token), op, left, right);
	}
}

rooibos:
	statements EOF
	;

block:
	'{' statements '}'
	;

statements:
	statement*
	;

statement:
	Id
	| functionDeclaration
	| varDeclaration
	| assignment
	| returnStatement
	| callStatement
	| ifStatement
	| classDeclaration
	| typeAlias
	;

classDeclaration:
	'class' Id 
	{
		Class klass = new Class(new Source($Id), $Id.text);
	}
	(
		'extends' typeAtom
		{
			if ($typeAtom.t != null) klass.superType = $typeAtom.t;
		}
	)?
	{
		scope.add(klass);
		scope.enter(klass);
	}
	'{'
		classStatement[klass]*
		constructorDeclaration[klass]?
		classStatement[klass]*
	'}'
	{
		scope.leave(klass);
		klass.finish();
	}
	;
	
typeAlias:
	'type' Id '=' type
	{
		TypeAlias alias = new TypeAlias(new Source($Id), $Id.text, $type.t);
		scope.add(alias);
		
		TypeAliasStatement aliasStatement = new TypeAliasStatement(new Source($Id), alias);
		scope.add(aliasStatement);
	}
	;
	
classStatement[Class klass]:
	methodDeclaration[klass]
	| fieldDeclaration[klass]
	;
	
constructorDeclaration[Class klass] returns [Constructor constructor]:
	f='constructor'
	{
		Constructor constr = new Constructor(new Source($f), $klass);
		klass.constructor = constr;
		scope.enter(constr);
	}
	'(' (parameter[constr] (',' parameter[constr])*)? ')' block
	{
		scope.leave(constr);
		$constructor = constr;
	}
	;

methodDeclaration[Class klass] returns [Method method]:
	modifiers f='method' Id
	{
		Method meth = new Method(new Source($f), $Id.text, $klass);
		meth.modifiers.addAll($modifiers.mods);		
		scope.enter(meth);
	}
	'(' (parameter[meth] (',' parameter[meth])*)? ')' (rt=':' type)? block
	{
		meth.returnType = $rt != null ? $type.t : Type.TOP;
		scope.leave(meth);
		$method = meth;
	}
	;

functionDeclaration returns [Function function]:
	f='function' Id
	{
		Function func = new Function(new Source($f), $Id.text);
		scope.enter(func);
	}
	'(' (parameter[func] (',' parameter[func])*)? ')' (rt=':' type)? block
	{
		func.returnType = $rt != null ? $type.t : Type.TOP;
		scope.leave(func);
		$function = func;
	}
	;
	
parameter[Function func]:
	Id ':' type
	{
		Parameter parameter = new Parameter(new Source($Id.text), $type.t, $Id.text);
		func.addParameter(parameter);
	}
	;
	
fieldDeclaration[Class klass]:
	{boolean isConst = false;}
	('var' | 'val' {isConst = true;}) 
	fieldParts+=fieldPart (',' fieldParts+=fieldPart)* 
	(':' type 
		{
			for (FieldPartContext fieldPart : $fieldParts) fieldPart.fld.type = $type.t;
		}
	)? eol
	{
		for (FieldPartContext fieldPart : $fieldParts) {
			Field field = fieldPart.fld;
			if (isConst) field.setConst();
			scope.add(field);
		}
	}
	;

varDeclaration:
	{boolean isConst = false;}
	('var' | 'val' {isConst = true;}) 
	varParts+=varPart (',' varParts+=varPart)* 
	(':' type 
		{
			for (VarPartContext varPart : $varParts) varPart.var.type = $type.t;
		}
	)? eol?
	{
		for (VarPartContext varPart : $varParts) {
			Variable var = varPart.var;
			if (isConst) var.setConst();
			scope.add(var);
		}
	}
	;
	
fieldPart returns [Field fld]: 
	Id (e='=' expression)?
	{
		Field field = new Field(new Source($Id), Type.INVALID, $Id.text);
		field.initial = $e == null ? null : $expression.value;
		if (field.initial != null) field.type = field.initial.type;
		$fld = field;
	}
	;	

varPart returns [Variable var]: 
	Id (e='=' expression)?
	{
		Variable variable = new Variable(new Source($Id), Type.INVALID, $Id.text);
		variable.initial = $e == null ? null : $expression.value;
		if (variable.initial != null) variable.type = variable.initial.type;
		$var = variable;
	}
	;

ifStatement:
	ifkeyword='if'
	{
		IfStatement ifStat = new IfStatement(new Source($ifkeyword));
		scope.enter(ifStat);
	}
	'(' condition last=')'
	{
		ifStat.setCondition($condition.value);
		Scope ifPart = new Scope($last, "ifpart-"+Node.genID());
		ifStat.setIfPart(ifPart);
		scope.enter(ifPart);
	}
	block
	{
		scope.leave(ifPart);
		scope.leave(ifStat);
	}
	;

assignment:
	variableValue eq='=' expression eol
	{
		Assignment assign = new Assignment(new Source($eq));
		scope.add(assign);
		
		assign.left = $variableValue.value;
		assign.right = $expression.value;
	}
	;

returnStatement:
	r='return' expression eol
	{
		Return ret = new Return(new Source($r));
		ret.value = $expression.value;
		scope.add(ret);
	}
	;
	
modifiers returns [Set<Modifier> mods]:
	{$mods = new TreeSet<Modifier>();}
	(modifier {$mods.add($modifier.value);})*
	;
	
modifier returns [Modifier value]:
	'static' {$value = Modifier.STATIC;}
	;
	
condition returns [Value value]:
	expression {$value = $expression.value;}
	;
	
expression returns [Value value]:
	a=polynomial op='==' b=polynomial {$value = binaryOp($op, BinaryOp.Operation.EQUAL, $a.value, $b.value);}
	| polynomial {$value = $polynomial.value;}
	;	

polynomial returns [Value value]:
	a=polynomial op='+' b=term {$value = binaryOp($op, BinaryOp.Operation.ADD, $a.value, $b.value);}
	| a=polynomial op='-' b=term {$value = binaryOp($op, BinaryOp.Operation.SUB, $a.value, $b.value);}
	| term {$value = $term.value;}
	;

term returns [Value value]:
	a=term op='/' b=atom {$value = binaryOp($op, BinaryOp.Operation.DIV, $a.value, $b.value);}
	| a=term op='*' b=atom {$value = binaryOp($op, BinaryOp.Operation.MUL, $a.value, $b.value);}
	| atom {$value = $atom.value;}
	;

atom returns [Value value]:
	'(' expression ')' {$value = $expression.value;}
	| call {$value = $call.value;}
	| variableValue {$value = $variableValue.value;}
	| Integer {$value = new Literal(new Source($Integer), scope.findType("int"), Long.parseLong($Integer.text)); $value.setConst();}
	| Float {$value = new Literal(new Source($Float), scope.findType("float"), Double.parseDouble($Float.text)); $value.setConst();}
	| String {$value = new Literal(new Source($String), scope.findType("string"), $String.text); $value.setConst();}
	;

callStatement:
	call eol
	{
		Value value = $call.value;
		if (value == null) value = new Value(Source.UNKNOWN, Type.INVALID);
		Statement stat = new Statement(value.source, value.name, value);
		scope.add(stat);
	}
	;

call returns [Value value]:
	scoping Id
	{
		Call callValue = null;
		Node node = $scoping.currentScope.find($Id.text);
				
		if (node instanceof Method) {
			callValue = new MethodCall(new Source($Id), (Method)node);
		}
		else if (node instanceof Function) {
			callValue = new FunctionCall(new Source($Id), (Function)node);
		}
		else if (node instanceof Class) {
			callValue = new ConstructorCall(new Source($Id), (Class)node);
		}
		else {
			String errMsg = "Could not find '" + $Id.text + "'";
			if ($scoping.currentScope != scope.current) errMsg += " in '" + $scoping.currentScope.name + "'";
			reporter.error(new Source($Id), errMsg);
			$value = new Value(new Source($Id), Type.INVALID);
		}
	}
	'(' (argument[callValue] (',' argument[callValue])*)? ')'
	{
		$value = callValue;
		
		if ($value == null) $value = new Value(new Source($Id), Type.INVALID);
		
		if ($scoping.nodes.peek() == scope.current) $scoping.nodes.pop();
		while (!$scoping.nodes.isEmpty()) {
			node = $scoping.nodes.pop();
			if (node == null) continue;
			$value = new ScopeAccess($value.source, node, $value);
		}
	}
	;
	
argument[Call callValue]:
	expression {if (callValue != null) callValue.arguments.add($expression.value);}
	;

variable returns [Variable var]:
	Id
	{
		$var = scope.findVariable($Id.text);
		if ($var == null) {
			reporter.error(new Source($Id), "Could not find '" + $Id.text + "'");
			$var = Variable.INVALID;
		}
	}
	;

variableValue returns [VariableValue value]:
	scoping Id
	{	
		Variable var = $scoping.currentScope.findVariable($Id.text);
		if (var == null) {
			reporter.error(new Source($Id), "Unknown variable '" + $Id.text + "'");
			$value = new VariableValue(new Source($Id), Variable.INVALID);
		}
		else {
			$value = new VariableValue(new Source($Id), var);

			if ($scoping.nodes.peek() == scope.current) $scoping.nodes.pop();
			
			while (!$scoping.nodes.isEmpty()) {
				Node node = $scoping.nodes.pop();
				if (node == null) continue;
				$value = new VariableScopeAccess($value.source, node, $value);
			}
		}
	}
	;

type returns [Type t]:
	{
		UnionType ut = new UnionType("[union type]");
	}
	typeAtom '|' b=type {ut.addType($typeAtom.t); ut.addType($b.t);}
	{
		$t = ut;
	}
	| '(' type ')' {$t = $type.t;}
	| typeAtom {$t = $typeAtom.t;}
	;

typeAtom returns [Type t]:
	Id
	{
		$t = scope.findType($Id.text);
		if ($t == null) {
			reporter.error(new Source($Id), "Unknown type '" + $Id.text + "'");
			$t = Type.TOP;
		}
	}
	;
	
scoping returns [LinkedList<Node> nodes, Scope currentScope]
		@init {
			$nodes = new LinkedList<Node>();
			Node curNode = scope.current;
			$currentScope = scope.current;
		}:
	(
		Id
		{
			Scope prevScope = (Scope)curNode;
			
			if ($Id.text.equals("this")) {
				Type type = scope.getEnclosingType();
				curNode = type;
				$nodes.add(type);
			}
			else {
				curNode = prevScope.find($Id.text);
				if (curNode == null) 
					reporter.error(new Source($Id), "Could not find '" + $Id.text + "' in scope of '" + prevScope + "'");
				else
					$nodes.add(curNode);
			}
		}
		'.'
	)*
	{
		if ($nodes.isEmpty()) $nodes.add(scope.current);
			
		curNode = $nodes.getLast();
		if (curNode instanceof Scope) {
			$currentScope = (Scope)curNode;
		}
		else if (curNode instanceof Variable) {
			$currentScope = ((Variable)curNode).type;
		}
		else
			reporter.error(new Source($Id), "'" + curNode + "' does not provide scope");
	}
	;

eol: ';'? ;

Id:
	[a-zA-Z] [a-zA-Z0-9_]*
	;

Integer : Digit+;
Float : (Digit+ '.' Digit* | '.' Digit+);
String : '"' ( EscapeSequence | ~('\\'|'"') )* '"' ;

fragment EscapeSequence : 
	'\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
	| UnicodeEscape
	| OctalEscape
	;

fragment OctalEscape :
	'\\' ('0'..'3') ('0'..'7') ('0'..'7')
	| '\\' ('0'..'7') ('0'..'7')
	| '\\' ('0'..'7')
	;

fragment UnicodeEscape : '\\' 'u' HexDigit HexDigit HexDigit HexDigit;
 
fragment HexDigit : (Digit|'a'..'f'|'A'..'F');
fragment Digit : [0-9];

WS: [ \n\t\r\u000C]+ -> channel(HIDDEN);