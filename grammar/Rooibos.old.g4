grammar Rooibos;

@header {
	import java.util.Stack;
	import java.util.Queue;
	import java.util.LinkedList;
}

@lexer::members {
	private Stack<Integer> indentLevels = new Stack<Integer>();
	private Queue<Token> tokens = new LinkedList<Token>();
	private boolean lastNewLine = false;

	@Override
	public void emit(Token t) {
		this._token = t;
		tokens.offer(t);
	}

	@Override
	public Token nextToken() {
		super.nextToken();

		Token t = tokens.poll();
		if (t.getType() == Token.EOF) {
			jump(NewLine);
			System.out.println("HOLD " + indentLevels.size());
			while (!indentLevels.isEmpty()) {
				indentLevels.pop();
				jump(Dedent);
			}
			jump(NewLine);
			emit(t);
			t = tokens.poll();
		}

		System.out.println("PASS " + t);
		return t;
	}

	private void jump(int ttype) {
		String name = "";
		switch (ttype) {
			case Dedent: name = "level=" + indentLevels.size() + "-down"; break;
			case Indent: name = "level=" + indentLevels.size() + "-up"; break;
			case NewLine: name = "newline"; break;
		}

		CommonToken t = new CommonToken(ttype, name);
		t.setLine(getLine());
		t.setCharPositionInLine(getCharPositionInLine());
		emit(t);
	}
}

rooibos:
	statements EOF
	;

block:
	Indent statements Dedent
	;

statements:
	statement*
	;

statement:
	Id
	| function
	| varDeclaration
	| constDeclaration
	| assignment
	| functionCall
	;

function:
	'function' Id '(' ')' block
	;

varDeclaration:
	'var' Id (',' Id)* ':' Id endOfLine
	;

constDeclaration:
	'const' Id ('=' Integer)? ':' Id endOfLine
	;

assignment:
	Id '=' Integer endOfLine
	;

functionCall:
	Id '(' Id ')' endOfLine
	;

endOfLine:
	NewLine+
	;

NewLine:
	(NL SP?)* NL SP?
	{
		int prevLevel = indentLevels.isEmpty() ? 0 : indentLevels.peek();
		int level = 0;

		String indentation = getText();
		while (indentation.charAt(indentation.length() - 1 - level) == '\t')
			level++;

		if (level > prevLevel) {
			jump(Indent);
			indentLevels.push(level);
		}
		else if (level < prevLevel) {
			jump(NewLine);

			System.out.println("Down: " + level + " " + prevLevel);
			while (level < prevLevel) {
				indentLevels.pop();
				jump(Dedent);
				prevLevel = indentLevels.isEmpty() ? 0 : indentLevels.peek();
			}
		}
		else {
			setText("newline");
		}
	}
	;

Id:
	('a'..'z' | 'A'..'Z')+
	;

Integer : [0-9]+;
Float : [0-9]+ '.' [0-9]* ;

SpaceChars:
	SP -> channel(HIDDEN)
	;

/* Dummy rules to force tokens */
Indent: ' ';
Dedent: ' ';

fragment NL: '\r'? '\n' | '\r';
fragment SP: (' ' | '\t')+;